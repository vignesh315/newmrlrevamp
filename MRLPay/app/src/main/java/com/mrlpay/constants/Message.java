package com.mrlpay.constants;

/**
 * Created by Vignesh on 06-10-2016.
 */

public interface Message {
     String HISTORY = "HISTORY";
     String REPORTS = "Reports";
     String TIP_AND_TAX = "Tip & Tax";
     String ITEMS = "ITEMS";
     String PROFILE = "Profile";
     String USER_SETTING = "User Settings";
     String CHANGE_PASSWORD = "Change Password";
     String SUPPORT = "Support";
     String NO_SALE = "No Sale";
     String KEYPAD = "KEYPAD";
     String LIBRARY = "LIBRARY";
     String DOLLAR="$";
     String EMPTY="";
     String MINUS="-";
     String ST_ZERO="0";
     String ST_ONE="1";
     String ST_TWO="2";
     String ST_THREE="3";
     String ST_FOUR="4";
     String ST_FIVE="5";
     String ST_SIX="6";
     String ST_SEVEN="7";
     String ST_EIGHT="8";
     String ST_NINE="9";
     String COMMA=",";
     String FULLSTOP=".";
     String FRISTCHECK="$0.0";
     String SECONDCHECK="$0.";
     String THRIDCHECK=".0";
     String INITIAL="$0.00";
     String FINALAMT="$999,999.99";
     String INDIANCURRFORMAT="#####,###.##";
     String INDIANCURREFORMATONE="#####,##0.00";
     String DEFAULTCURRENCYFORMAT="0.00";
     String CURRENT_SALE="Current Sale";
     String SALE="Sale";
     String ADD_NOTE="Add Note";
     String TILDA="~";
     String CUSTOM_AMOUNT="Custom Amount";
     String ALL_ITEMS="All Items";
     String CLEAR_ALL="Clear All";
     String DISCOUNT="Discounts";
     String CHARGE="Charge";
     String ONE_LINE_SPACE=" ";
     String PLAIN_FINAL_AMT="99999999";
     String ITEM_ORDER_TAG="Total :";
     String QUICK_SALE="Q";
     String LIBRARY_SALE="L";
     String DISCOUNT_ON_SALE="D";
     String TAXABLE="Y";
     String NOT_TAXABLE="N";
     String ST_SEVEN_NINES="9999999";
     String NO_DATA_AVAIL="NO_DATA_AVAIL";
     String SET_PRICE="Set Price";
     String SAVE="Save";
     String NULL="null";
     String HEADING_HISTORY="History";
     String HEADING_REPORTS="Report";



     int ZERO=0;
     int ONE=1;
     int TWO=2;
     int THREE=3;
     int FOUR=4;
     int FIVE=5;
     int SIX=6;
     int SEVEN=7;
     int EIGHT=8;
     int NINE=9;
     int TEN=10;
     int ELEVEN=11;
     int HUNDRED=100;
     int SEVEN_NINES=9999999;
     int MINUS_ONE=-1;

     Double D_ZERO=0.0;

     //Color String
     String TRANSLUCENT="#99000000";
     String COLOR_NAVIG = "#F2F4F5";

     //Shared Preference Constants
     String ITEM_NAME= "itemName";
     String ITEM_CUSTOM_NOTE="itemCustomNote";
     String ITEM_AMOUNT="itemAmount";
     String ITEM_QUATITY="itemQuatity";
     String ITEM_COUNT="itemcount";
     String ITEM_DISCOUNT="itemdiscount";
     String ITEM_DISCOUNT_AMT="itemdiscountamount";
     String ITEM_TOTAL_AMT="itemtotalamt";
     String ITEM_SALE_TYPE="saletype";
     String ITEM_TAXABLE="itemtaxable";
     String IS_ITEM_TAXABLE="isitemtaxable";
     String SINGLE_ITEM_TAX_CHK="singleitemtaxchk";
     String CURRENT_ITEM_POSITION="currentposition";
     String SELECTED_ITEM_NAME="selecteditemname";
     String CURRENT_ITEM_SALE_TYPE="currentitemsaletype";
     String ALTER_ITEM_PRICE_SUM="alteritempricesum";
     String ALTER_ITEM_QUATITY_SUM="alteritemquatsum";
     String TOTAL_DISCOUNT_ON_ITEM="totaldiscountonitem";

     //Report date select range

     String START_DATE_SEL="Start Date";
     String END_DATE_SEL="End Date";
     String START_TIME_SEL="Start Time";
     String END_TIME_SEL="End Time";

     //Separator
     String TILDE="~";
     String CARET="^";
     //card
     String AMEX="amex";
     String DISCOVER="discover";
     String DINER="diner";
     String MASETRO="masetro";
     String MASTER="master";
     String VISA="visa";
     String CUSTOM_ITEM_NAME="Custom Item name";


     String TYPE_IMAGE="I";
     String TYPE_COLOR="C";

     String TAG="MRL";

     //dialog tag
     String DATEPICKERDIALOG="Datepickerdialog";
     String TIMEPICKERDIALOG= "Timepickerdialog";
}
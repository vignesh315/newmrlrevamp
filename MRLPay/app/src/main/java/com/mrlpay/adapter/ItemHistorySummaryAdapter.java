package com.mrlpay.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrlpay.R;

import java.util.List;

import static com.mrlpay.constants.Message.TYPE_IMAGE;

/**
 * Created by Vignesh on 15-12-2016.
 */

public class ItemHistorySummaryAdapter extends RecyclerView.Adapter<ItemHistorySummaryAdapter.MyViewHolder>{
    private List<Item> itemList;
    private int itemPosition;
    private Context context;
    private final OnItemClickListener listener;
    private Typeface typeface;

    public ItemHistorySummaryAdapter(List<Item> itemList,Typeface typeface ,OnItemClickListener listener) {
        this.itemList = itemList;
        this.listener = listener;
        this.typeface = typeface;
    }
    public interface OnItemClickListener {
        void onItemClick(Item item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageName;
        private TextView indexing,itemname,price,quatity;
        private String imageIdexing;
        protected MyViewHolder(View itemView) {
            super(itemView);
            imageName = (ImageView)itemView.findViewById(R.id.indexing);
            indexing = (TextView)itemView.findViewById(R.id.indexname);
            itemname = (TextView)itemView.findViewById(R.id.itemname);
            price = (TextView)itemView.findViewById(R.id.price);
            indexing.setTypeface(typeface);
            itemname.setTypeface(typeface);
            price.setTypeface(typeface);
        }
        public void bind(final Item item, final ItemHistorySummaryAdapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.libray_items,parent,false);
        return new ItemHistorySummaryAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item item = itemList.get(position);
        if (TYPE_IMAGE.equals(item.getImageIndexing().toString()))
            holder.imageName.setImageResource(item.getImageName());
        else
            holder.imageName.setBackgroundColor(item.getImageName());
        holder.indexing.setText(item.getIndexing());
        holder.itemname.setText(item.getItemName());
        holder.price.setText(item.getPrice());
        holder.bind(itemList.get(position),listener);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}

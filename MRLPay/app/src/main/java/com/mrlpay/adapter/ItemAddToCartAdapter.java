package com.mrlpay.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrlpay.R;

import java.util.List;

import static com.mrlpay.constants.Message.EMPTY;
import static com.mrlpay.constants.Message.NO_DATA_AVAIL;
import static com.mrlpay.constants.Message.ST_ONE;

/**
 * Created by Vignesh on 23-11-2016.
 */

public class ItemAddToCartAdapter extends RecyclerView.Adapter<ItemAddToCartAdapter.ViewHolderItem>{
    private List<ItemData> itemList;
    private int itemPosition;
    private Context context;
    private final ItemAddToCartAdapter.OnItemClickListener listener;
    private Typeface typeface;

    public interface OnItemClickListener {
        void onItemClick(ItemData itemData,int position);
    }
    public ItemAddToCartAdapter(List<ItemData> itemList,Typeface typeface, OnItemClickListener listener) {
        this.itemList = itemList;
        this.listener = listener;
        this.typeface = typeface;
    }
    public class ViewHolderItem extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imageName;
        public TextView itemname,price,quatity,multiply,customNote;
        public ViewHolderItem(View itemView) {
            super(itemView);
            itemname = (TextView)itemView.findViewById(R.id.itemname);
            price = (TextView)itemView.findViewById(R.id.price);
            multiply= (TextView)itemView.findViewById(R.id.item_mul);
            quatity=(TextView)itemView.findViewById(R.id.itemquatity);
            customNote=(TextView)itemView.findViewById(R.id.itemnote);
            itemname.setTypeface(typeface);
            price.setTypeface(typeface);
            multiply.setTypeface(typeface);
            quatity.setTypeface(typeface);
            customNote.setTypeface(typeface,Typeface.ITALIC);
        }
        public void bind(final ItemData itemData, final ItemAddToCartAdapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    int position = getAdapterPosition();
                    listener.onItemClick(itemData,position);
                }
            });
        }

        @Override
        public void onClick(View v) {

        }
    }
    @Override
    public ItemAddToCartAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item_tocart,parent,false);
        return new ItemAddToCartAdapter.ViewHolderItem(view);
    }

    @Override
    public void onBindViewHolder(ItemAddToCartAdapter.ViewHolderItem holder, int position) {
        ItemData itemData = itemList.get(position);
        holder.itemname.setText(itemData.getItemName());
        String customNote= itemData.getCustomNode();
        if (!NO_DATA_AVAIL.equals(customNote)&& customNote!=null ){
            holder.customNote.setVisibility(View.VISIBLE);
            holder.customNote.setText(itemData.getCustomNode());
        }else
            holder.customNote.setVisibility(View.GONE);
        holder.price.setText(itemData.getPrice());
        String tempQuatity = itemData.getQuatity();
        holder.quatity.setText(itemData.getQuatity());
        if (ST_ONE.equals(tempQuatity)) {
            holder.quatity.setTextColor(Color.WHITE);
            holder.multiply.setTextColor(Color.WHITE);
        }else{
            holder.quatity.setTextColor(Color.LTGRAY);
            holder.multiply.setTextColor(Color.LTGRAY);
        }
        holder.bind(itemList.get(position),listener);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
    public void swap(List<ItemData> itemDatas){
        itemDatas.clear();
        itemDatas.addAll(itemDatas);
        notifyDataSetChanged();
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getItemPosition() {
        return itemPosition;
    }

    public void setItemPosition(int itemPosition) {
        this.itemPosition = itemPosition;
    }
}

package com.mrlpay.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.mrlpay.R;
import com.mrlpay.pageviewer.CustomPagerAdapter;

import java.lang.reflect.Type;
import java.util.List;

import static com.mrlpay.constants.Message.TYPE_IMAGE;

/**
 * Created by Vignesh on 30-09-2016.
 */

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.MyViewHolder> {
    private List<Item> itemList;
    private int itemPosition;
    private Context context;
    private final OnItemClickListener listener;
    private Typeface typeface;
    public interface OnItemClickListener {
        void onItemClick(Item item);
    }
    //private View animateView[];
    //View animView[];
    //TranslateAnimation[] translateAnimations;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageName;
        public TextView indexing,itemname,price,quatity;
        public MyViewHolder(View itemView) {
            super(itemView);
            imageName = (ImageView)itemView.findViewById(R.id.indexing);
            indexing = (TextView)itemView.findViewById(R.id.indexname);
            itemname = (TextView)itemView.findViewById(R.id.itemname);
            price = (TextView)itemView.findViewById(R.id.price);
            indexing.setTypeface(typeface);
            itemname.setTypeface(typeface);
            price.setTypeface(typeface);
        }
        public void bind(final Item item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public ItemListAdapter(List<Item> itemList, Typeface typeface, OnItemClickListener listener){
        this.itemList = itemList;
        this.listener = listener;
        this.typeface=typeface;
    }

    @Override
    public ItemListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.libray_items,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemListAdapter.MyViewHolder holder, final int position) {
        Item item = itemList.get(position);
        if (TYPE_IMAGE.equals(item.getImageIndexing().toString()))
            holder.imageName.setImageResource(item.getImageName());
        else
            holder.imageName.setBackgroundColor(item.getImageName());
        holder.indexing.setText(item.getIndexing());
        holder.itemname.setText(item.getItemName());
        holder.price.setText(item.getPrice());
        holder.bind(itemList.get(position),listener);
    }
    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public int getItemPosition() {
        return itemPosition;
    }

    public void setItemPosition(int itemPosition) {
        this.itemPosition = itemPosition;
    }

    public Context getContext() {
        return context;
    }
    public void swap(List<Item> data){
        data.clear();
        data.addAll(data);
        notifyDataSetChanged();
    }

    public void setContext(Context context) {
        this.context = context;
    }
}

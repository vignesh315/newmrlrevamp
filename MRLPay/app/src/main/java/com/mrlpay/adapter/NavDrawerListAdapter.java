package com.mrlpay.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.mrlpay.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.mrlpay.constants.Message.CHANGE_PASSWORD;
import static com.mrlpay.constants.Message.HISTORY;
import static com.mrlpay.constants.Message.ITEMS;
import static com.mrlpay.constants.Message.PROFILE;
import static com.mrlpay.constants.Message.REPORTS;
import static com.mrlpay.constants.Message.SUPPORT;
import static com.mrlpay.constants.Message.TIP_AND_TAX;
import static com.mrlpay.constants.Message.USER_SETTING;

public class NavDrawerListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<NavDrawerItem> navDrawerItems;
	//Macro macro=new Macro();
	String[] setting_options = {HISTORY,
								REPORTS,
								TIP_AND_TAX,
								ITEMS,
								PROFILE,
								USER_SETTING,
								CHANGE_PASSWORD,
								SUPPORT};
	int setting_options_images[] = {R.drawable.icon_currentbatch,
									R.drawable.icon_viewreports,
									R.drawable.icon_tipandtax,
									R.drawable.icon_item,
									R.drawable.icon_profile,
									R.drawable.icon_usersetting,
									R.drawable.icon_chngpwd,
									R.drawable.icon_support};

	List<String> settingHeader;
	HashMap<String, List<String>> settingChildlist;
	Activity activity;
	public static int width;
	public NavDrawerListAdapter(Context context,
								ArrayList<NavDrawerItem> navDrawerItems, Activity activity) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
		this.activity=activity;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.drawer_list_item, null);
		}

		RelativeLayout relDrawerLayout = (RelativeLayout) convertView
				.findViewById(R.id.drawerlayout);
		relDrawerLayout.setGravity(Gravity.CENTER_HORIZONTAL);
		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
		Typeface face= Typeface.createFromAsset(activity.getAssets(),"fonts/calibri.otf");
		txtTitle.setTypeface(face);
		//final ExpandableListView spinner= (ExpandableListView) convertView.findViewById(R.id.spinner);
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		width = metrics.widthPixels;

		ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) txtTitle
				.getLayoutParams();
		/*if (position == 0)
		{
			if(width>540) {
				mlp.setMargins(13, 0, 0, 0);
			}else{
				mlp.setMargins(13, 0, 0, 0);
			}
			txtTitle.setText(navDrawerItems.get(position).getTitle());
			imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
		}else if(position == 1){
			//spinner.setVisibility(View.GONE);
			txtTitle.setVisibility(View.VISIBLE);
			imgIcon.setVisibility(View.VISIBLE);

			if(width>540) {
				mlp.setMargins(75, 0, 0, 0);
			}else{
				mlp.setMargins(60, 0, 0, 0);
			}

			txtTitle.setText(navDrawerItems.get(position).getTitle());
			imgIcon.setImageResource(navDrawerItems.get(position).getIcon());

		}

		else if (position == 2) {
			txtTitle.setVisibility(View.VISIBLE);
			imgIcon.setVisibility(View.VISIBLE);

			if(width>540) {
				mlp.setMargins(75, 0, 0, 0);
			}else{
				mlp.setMargins(60, 0, 0, 0);
			}

			txtTitle.setText(navDrawerItems.get(position).getTitle());
			imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
			*//*txtTitle.setVisibility(View.GONE);
			imgIcon.setVisibility(View.GONE);
			//spinner.setVisibility(View.VISIBLE);
			//settingOptions();
			spinner.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
				@Override
					public void onGroupExpand(int groupPosition) {
					ViewGroup.LayoutParams params = spinner.getLayoutParams();
					if(width>540 && width<1000) {
						params.height = 540;//850;
						spinner.setLayoutParams(params);
						spinner.requestLayout();
					}else if(width>1000){
						params.height =850;
						spinner.setLayoutParams(params);
						spinner.requestLayout();
					}else{
						params.height = 460;
						spinner.setLayoutParams(params);
						spinner.requestLayout();
					}
				}

			});*//*
			*//*spinner.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
				@Override
				public void onGroupCollapse(int i) {
					ViewGroup.LayoutParams params1 = spinner.getLayoutParams();
					if(width>1000){
						params1.height =100;
						spinner.setLayoutParams(params1);
						spinner.requestLayout();
					}else {

						params1.height = 70;//100;//70;
						spinner.setLayoutParams(params1);
						spinner.requestLayout();
					}

				}
			});*//*

		}else if (position == 4){
			//spinner.setVisibility(View.GONE);
			txtTitle.setVisibility(View.VISIBLE);
			imgIcon.setVisibility(View.VISIBLE);
			*//*ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) txtTitle
					.getLayoutParams();*//*
			if(width>540) {
				mlp.setMargins(62, 0, 0, 0);
			}else{
				mlp.setMargins(50, 0, 0, 0);
			}

			txtTitle.setText(navDrawerItems.get(position).getTitle());
			imgIcon.setImageResource(navDrawerItems.get(position).getIcon());


		}else{*/
			//spinner.setVisibility(View.GONE);
			txtTitle.setVisibility(View.VISIBLE);
			imgIcon.setVisibility(View.VISIBLE);
		/*	ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) txtTitle
					.getLayoutParams();*/

			if(width>540) {
				mlp.setMargins(42, 0, 0, 0);
			}else{
				mlp.setMargins(35, 0, 0, 0);
			}
			txtTitle.setText(navDrawerItems.get(position).getTitle());
			imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
		//}

		return convertView;
	}
	private int GetDipsFromPixel(float pixels) {
		// Get the screen's density scale
		final float scale = activity.getResources().getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}
	private void settingOptions() {
		settingHeader = new ArrayList<String>();
		settingChildlist = new HashMap<String, List<String>>();

		// Adding child data
		settingHeader.add("SETTINGS");

		// Adding child data
		List<String> settingchild = new ArrayList<String>();
		settingchild.add(" PIN");
		settingchild.add("CONTACT");
		settingchild.add("EMAIL");
		settingchild.add("MOBILE");
		settingChildlist.put(settingHeader.get(0), settingchild); // Header, Child data


	}

	// Adapter class for spinner control
	public class MyAdapter extends ArrayAdapter<String> {

		public MyAdapter(Context context, int textViewResourceId, String[] objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public View getDropDownView(int position, View convertView, ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}

		public View getCustomView(int position, View convertView, ViewGroup parent) {

			LayoutInflater inflater=(LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			View row=inflater.inflate(R.layout.setting_spinner_listrow, parent, false);

			TextView sub=(TextView)row.findViewById(R.id.title);
			sub.setText(setting_options[position]);

			ImageView icon=(ImageView)row.findViewById(R.id.icon);
			icon.setImageResource(setting_options_images[position]);
			return row;
		}
	}
}

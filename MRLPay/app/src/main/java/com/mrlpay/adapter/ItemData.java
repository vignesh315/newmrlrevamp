package com.mrlpay.adapter;

import java.util.Comparator;

public class ItemData implements Comparable<String> {
	private String itemName,customNode,price,quatity,saleType,isTaxable;
	

	public ItemData(String itemName,String customNode, String price, String quatity,String saleType,String isTaxable) {
		// TODO Auto-generated constructor stub
		this.itemName = itemName;
		this.price = price;
		this.quatity = quatity;
		this.saleType = saleType;
		this.isTaxable=isTaxable;
		this.customNode=customNode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getQuatity() {
		return quatity;
	}

	public void setQuatity(String quatity) {
		this.quatity = quatity;
	}

	public String getSaleType() {
		return saleType;
	}

	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}

	public String getIsTaxable() {
		return isTaxable;
	}

	public void setIsTaxable(String isTaxable) {
		this.isTaxable = isTaxable;
	}

	public String getCustomNode() {
		return customNode;
	}

	public void setCustomNode(String customNode) {
		this.customNode = customNode;
	}

	@Override
	public int compareTo(String compareItemName) {
		// TODO Auto-generated method stub
		return 0;
	}
	public static Comparator<ItemData> ItemNameComparator = new Comparator<ItemData>() {

		@Override
		public int compare(ItemData itemNameOne, ItemData itemNameTwo) {
			// TODO Auto-generated method stub
			String itemFirst = itemNameOne.getItemName().toUpperCase();
		      String itemSecond = itemNameTwo.getItemName().toUpperCase();
			return itemFirst.compareTo(itemSecond);
		}
	};

}

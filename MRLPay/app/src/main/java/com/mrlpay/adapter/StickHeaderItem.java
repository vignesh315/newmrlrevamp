package com.mrlpay.adapter;

/**
 * Created by Arun on 09-12-2016.
 */

public class StickHeaderItem {
    private String tranIcon;
    private String itemSummary;
    private String itemName;
    private String transTime;
    private String saleindicatior;

    public StickHeaderItem(String tranIcon, String itemSummary, String itemName, String transTime, String saleindicatior) {
        this.tranIcon = tranIcon;
        this.itemSummary = itemSummary;
        this.itemName = itemName;
        this.transTime = transTime;
        this.saleindicatior = saleindicatior;
    }

    public String getTranIcon() {
        return tranIcon;
    }

    public void setTranIcon(String tranIcon) {
        this.tranIcon = tranIcon;
    }

    public String getItemSummary() {
        return itemSummary;
    }

    public void setItemSummary(String itemSummary) {
        this.itemSummary = itemSummary;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }

    public String getSaleindicatior() {
        return saleindicatior;
    }

    public void setSaleindicatior(String saleindicatior) {
        this.saleindicatior = saleindicatior;
    }
}

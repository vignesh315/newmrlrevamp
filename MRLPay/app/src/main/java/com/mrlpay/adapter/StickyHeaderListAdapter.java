package com.mrlpay.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.mrlpay.R;

import java.lang.reflect.Type;
import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

import static com.mrlpay.constants.Message.AMEX;
import static com.mrlpay.constants.Message.CARET;
import static com.mrlpay.constants.Message.CUSTOM_ITEM_NAME;
import static com.mrlpay.constants.Message.DINER;
import static com.mrlpay.constants.Message.DISCOVER;
import static com.mrlpay.constants.Message.MASETRO;
import static com.mrlpay.constants.Message.MASTER;
import static com.mrlpay.constants.Message.NO_SALE;
import static com.mrlpay.constants.Message.VISA;

/**
 * Created by Vignesh on 07-12-2016.
 */

public class StickyHeaderListAdapter extends BaseAdapter implements StickyListHeadersAdapter,SectionIndexer{
    private final Context mContext;
    //private String[] mCountries;
    private int[] mSectionIndices;
    private String[] mSectionLetters;
    private LayoutInflater mInflater;
    private String []mHistoryTranIcon;
    private String []mHistroyHeaderData;
    private String []mHistorySummaryData;
    private String []mHistoryTimeData;
    private Typeface typefaceFont;

    public StickyHeaderListAdapter(Context context,Typeface typeface) {
        mContext = context;
        typefaceFont=typeface;
        mInflater = LayoutInflater.from(context);
        mHistoryTranIcon= new String[]{"amex", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "visa", "masetro", "master", "discover", "diner"};
        mHistroyHeaderData= new String[]{"A^THURSDAY,DECEMBER 1,2016", "B^FRIDAY OCTOBER 21,2016", "B^FRIDAY OCTOBER 21,2016", "B^FRIDAY OCTOBER 21,2016", "B^FRIDAY OCTOBER 21,2016", "B^FRIDAY OCTOBER 21,2016", "B^FRIDAY OCTOBER 21,2016", "B^FRIDAY OCTOBER 21,2016", "C^TUESDAY OCTOBER 18,2016", "D^SATURDAY OCTOBER 15,2016", "D^SATURDAY OCTOBER 15,2016", "D^SATURDAY OCTOBER 15,2016"};
        mHistorySummaryData = new String[]{"A^$0.00", "B^Sale", "B^Sale", "B^No Sale", "B^No Sale", "B^No Sale", "B^No Sale", "B^Sale", "C^Sale", "D^Sale", "D^Sale", "D^Sale"};
        mHistoryTimeData =new String[]{"12.48", "12.48", "12.48", "2.48", "12.48", "12.48", "12.48", "12.48", "12.48", "12.48", "12.48", "12.48"};
        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
    }
    private int[] getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<>();
        char lastFirstChar = mHistroyHeaderData[0].charAt(0);
        sectionIndices.add(0);
        for (int i = 1; i < mHistroyHeaderData.length; i++) {
            if (mHistroyHeaderData[i].charAt(0) != lastFirstChar) {
                lastFirstChar = mHistroyHeaderData[i].charAt(0);
                sectionIndices.add(i);
            }
        }
        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }
        return sections;
    }

    private String[] getSectionLetters() {
        String[] letters = new String[mSectionIndices.length];
        for (int i = 0; i < mSectionIndices.length; i++) {
            letters[i] = mHistroyHeaderData[mSectionIndices[i]];
        }
        return letters;
    }
    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup) {
        HeaderViewHolder holder;

        if (view == null) {
            holder = new HeaderViewHolder();
            view = mInflater.inflate(R.layout.stickylistheader, viewGroup, false);
            holder.text = (TextView) view.findViewById(R.id.text1);
            view.setTag(holder);
        } else {
            holder = (HeaderViewHolder) view.getTag();
        }

        // set header text as first char in name
        String headerChar = mHistroyHeaderData[i].subSequence(mHistroyHeaderData[i].indexOf("^")+1,mHistroyHeaderData[i].length()).toString();
        holder.text.setText(headerChar);
        holder.text.setTypeface(typefaceFont);
        return view;
    }

    @Override
    public long getHeaderId(int i) {
        return mHistroyHeaderData[i].subSequence(0, 1).charAt(0);
    }

    @Override
    public int getCount() {
        return mHistroyHeaderData.length;
    }

    @Override
    public Object getItem(int position) {
        return  mHistorySummaryData[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.sticky_list_item_layout, parent, false);
            viewHolder.transicon =(ImageView)convertView.findViewById(R.id.transicon);
            viewHolder.amount = (TextView) convertView.findViewById(R.id.amount);
            viewHolder.itemname =(TextView) convertView.findViewById(R.id.itemname);
            viewHolder.time= (TextView) convertView.findViewById(R.id.time);
            viewHolder.arrow=(ImageView)convertView.findViewById(R.id.arrow);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String imageName = mHistoryTranIcon[position].toString();
        if (imageName.equals(AMEX)) {
            viewHolder.transicon.setImageResource(R.drawable.amex);
        }else if(imageName.equals(DISCOVER)) {
            viewHolder.transicon.setImageResource(R.drawable.discover);
        }else if(imageName.equals(DINER)) {
            viewHolder.transicon.setImageResource(R.drawable.diner);
        }else if(imageName.equals(MASETRO)) {
            viewHolder.transicon.setImageResource(R.drawable.masetro);
        }else if(imageName.equals(MASTER)) {
            viewHolder.transicon.setImageResource(R.drawable.master);
        }else if(imageName.equals(VISA)) {
            viewHolder.transicon.setImageResource(R.drawable.visa);
        }
        if (!imageName.equals(NO_SALE)) {
            viewHolder.itemname.setVisibility(View.VISIBLE);
            viewHolder.itemname.setText(CUSTOM_ITEM_NAME);
            viewHolder.arrow.setImageResource(android.R.drawable.button_onoff_indicator_on);
        }
        viewHolder.amount.setText(mHistorySummaryData[position].subSequence(mHistorySummaryData[position].indexOf(CARET)+1,mHistorySummaryData[position].length()));
        viewHolder.time.setText(mHistoryTimeData[position]);
        viewHolder.amount.setTypeface(typefaceFont);
        viewHolder.itemname.setTypeface(typefaceFont);
        viewHolder.time.setTypeface(typefaceFont);
        return convertView;
    }

    @Override
    public Object[] getSections() {
        return mSectionLetters;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        if (mSectionIndices.length == 0) {
            return 0;
        }

        if (sectionIndex >= mSectionIndices.length) {
            sectionIndex = mSectionIndices.length - 1;
        } else if (sectionIndex < 0) {
            sectionIndex = 0;
        }
        return mSectionIndices[sectionIndex];
    }

    @Override
    public int getSectionForPosition(int position) {
        for (int i = 0; i < mSectionIndices.length; i++) {
            if (position < mSectionIndices[i]) {
                return i - 1;
            }
        }
        return mSectionIndices.length - 1;
    }
    public void clear() {
        mHistoryTranIcon = new String[0];
        mHistroyHeaderData = new String[0];
        mHistorySummaryData = new String[0];
        mHistoryTimeData = new String[0];
        mSectionIndices = new int[0];
        mSectionLetters = new String[0];
        notifyDataSetChanged();
    }
    public void restore() {
        mHistoryTranIcon= new String[]{"Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale"};
        mHistroyHeaderData= new String[]{"THURSDAY,DECEMBER 1,2016", "FRIDAY OCTOBER 21,2016", "FRIDAY OCTOBER 21,2016", "FRIDAY OCTOBER 21,2016", "FRIDAY OCTOBER 21,2016", "FRIDAY OCTOBER 21,2016", "FRIDAY OCTOBER 21,2016", "FRIDAY OCTOBER 21,2016", "TUESDAY OCTOBER 18,2016", "SATURDAY OCTOBER 15,2016", "SATURDAY OCTOBER 15,2016", "SATURDAY OCTOBER 15,2016"};
        mHistorySummaryData = new String[]{"$0.00", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale", "No Sale"};
        mHistoryTimeData =new String[]{"12.48", "12.48", "12.48", "2.48", "12.48", "12.48", "12.48", "12.48", "12.48", "12.48", "12.48", "12.48"};
        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
        notifyDataSetChanged();
    }

    class HeaderViewHolder {
        TextView text;
    }
    class ViewHolder{
        TextView amount,time,itemname;
        ImageView transicon,arrow;
    }
}

package com.mrlpay.adapter;

/**
 * Created by Vignesh on 30-09-2016.
 */

public class Item {
    private int imageName;
    private String imageIndexing,indexing,itemName,price,quatity;

    public Item() {
    }

    public Item(int imageName,String imageIdexing,String indexing,String itemName, String price,String quatity) {
        this.imageName= imageName;
        this.imageIndexing =imageIdexing;
        this.indexing=indexing;
        this.itemName = itemName;
        this.price = price;
        this.quatity = quatity;
    }

    public int getImageName() {
        return imageName;
    }

    public void setImageName(int imageName) {
        this.imageName = imageName;
    }

    public String getIndexing() {
        return indexing;
    }

    public void setIndexing(String indexing) {
        this.indexing = indexing;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuatity() {
        return quatity;
    }

    public void setQuatity(String quatity) {
        this.quatity = quatity;
    }

    public String getImageIndexing() {
        return imageIndexing;
    }

    public void setImageIndexing(String imageIndexing) {
        this.imageIndexing = imageIndexing;
    }
}

package com.mrlpay;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mrlpay.activity.postlogin.CustomizeReports;
import com.mrlpay.activity.postlogin.HistoryList;
import com.mrlpay.activity.postlogin.WelcomeActivity;
import com.mrlpay.activity.prelogin.FlashActivity;
import com.mrlpay.activity.prelogin.LoginActivity;
import com.mrlpay.adapter.NavDrawerItem;
import com.mrlpay.common.MyAlarmService;
import com.mrlpay.common.ProgressBar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.mrlpay.constants.Message.COLOR_NAVIG;
import static com.mrlpay.constants.Message.ONE;
import static com.mrlpay.constants.Message.THREE;
import static com.mrlpay.constants.Message.TWO;

public class MainActivity extends AppCompatActivity {
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private static final int RC_OVERLAY = 0;
    private BroadcastReceiver receiver = new ConnectionChangeReceiver();
    private static final String TAG=MainActivity.class.getName();
    ConnectivityManager connectivityManager;
    public static final int IDLE_DELAY_MINUTES = 15;
    NetworkInfo activeNetInfo;
    private PendingIntent pendingIntent;
    AlarmManager alarmManager;
    ImageButton imageButton,imageButtonDrawer;
    private String[] navMenuTitles={"welcome","transfer","billpay","logout"};
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    //Welcome activity
    TextView headerText,headerQuatityDis;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetInfo = connectivityManager.getActiveNetworkInfo();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            openOverlaySettings();
        }
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver, filter);
    }

    private void openOverlaySettings() {
        final Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getPackageName()));
        try {
            startActivityForResult(intent, RC_OVERLAY);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RC_OVERLAY:
                final boolean overlayEnabled = Settings.canDrawOverlays(this);
                // Do something...
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onUserInteraction() {
        Log.d(TAG, "User interaction to "+this.toString());
        super.onUserInteraction();
    }
    public void hideActionBar(){
        getSupportActionBar().hide();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if(receiver!= null){
                //unregisterReceiver(receiver);
                this.unregisterReceiver(this.receiver);
                receiver = null;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // your code
            minimizeApp();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /***Adding this Line for to get the store image to 64 bitencode format - Ends***/
    public boolean checkIfAlreadyhavePermission() {
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int coarse_Location = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int fine_Location = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int record_Audio = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        int read_Ext_Storage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int write_Ext_Storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int system_alert_window = ContextCompat.checkSelfPermission(this, Manifest.permission.SYSTEM_ALERT_WINDOW);
        if (camera == PackageManager.PERMISSION_GRANTED &&
                coarse_Location==PackageManager.PERMISSION_GRANTED
                && fine_Location==PackageManager.PERMISSION_GRANTED
                && record_Audio==PackageManager.PERMISSION_GRANTED
                && read_Ext_Storage==PackageManager.PERMISSION_GRANTED
                && write_Ext_Storage==PackageManager.PERMISSION_GRANTED
                && system_alert_window==PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,Manifest.permission.SYSTEM_ALERT_WINDOW}, 101);
    }
    public void startTimer() {
        Intent myIntent;
        myIntent = new Intent(MainActivity.this, MyAlarmService.class);
        pendingIntent = PendingIntent.getService(this, 0, myIntent, 0);
        alarmManager = (AlarmManager)getSystemService(this.ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND,900);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        //  Toast.makeText(this, "Alarm Set"+calendar.getTimeInMillis(), Toast.LENGTH_SHORT).show();
    }
    public void cancel(){
        if (alarmManager!=null)
            alarmManager.cancel(pendingIntent);
    }
    public void showDialogue(){

        final Dialog dialog = new Dialog(MainActivity.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);

        // Set dialog title
        // dialog.setTitle("Custom Dialog");

        // set values for custom dialog components - text, image and button
        Typeface face= Typeface.createFromAsset(this.getAssets(), "fonts/calibri.otf");
        TextView text = (TextView) dialog.findViewById(R.id.textDialog);
        text.setTypeface(face);
        //text.setText("Custom dialog Android example.");
        TextView image = (TextView) dialog.findViewById(R.id.imageDialog);
        image.setTypeface(face);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_PANEL);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView declineButton = (TextView) dialog.findViewById(R.id.declineButton);
        declineButton.setTypeface(face);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                Intent mainIntent = new Intent(MainActivity.this,
                        LoginActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(mainIntent);

            }
        });
    }
    public class ConnectionChangeReceiver extends BroadcastReceiver {


        public ConnectionChangeReceiver() {
        }

        /* (non-Javadoc)
         * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
         */
        public void onReceive(Context context, Intent intent )
        {
            connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            activeNetInfo = connectivityManager.getActiveNetworkInfo();
            if(activeNetInfo!= null){

            }else{
                Intent mainIntent = new Intent(MainActivity.this,
                        FlashActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(mainIntent);
            }
        }
    }

    @Override
    protected void onDestroy() {
        android.os.Process.killProcess(android.os.Process.myPid());
        super.onDestroy();
    }

    protected int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
    protected void moveDrawerToTop(int Layout) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DrawerLayout drawer = (DrawerLayout) inflater.inflate(Layout, null); // "null" is important.

        // HACK: "steal" the first child of decor view
        ViewGroup decor = (ViewGroup) getWindow().getDecorView();
        View child = decor.getChildAt(0);
        decor.removeView(child);
        LinearLayout container = (LinearLayout) drawer.findViewById(R.id.drawer_content); // This is the container we defined just now.
        container.addView(child, 0);
        drawer.findViewById(R.id.drawer_list).setPadding(0, getStatusBarHeight(), 0, 0);
        // Make the drawer replace the first child
        decor.addView(drawer);
    }
    protected Typeface getTypeface(){
        Typeface typeface;
        return typeface =Typeface.createFromAsset(getAssets(),"fonts/calibri.otf");
    }
    protected ArrayList<NavDrawerItem> getItems() {
        navDrawerItems = new ArrayList<NavDrawerItem>();
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);
        for (int i = 0; i < navMenuTitles.length; i++) {
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], navMenuIcons
                    .getResourceId(i, -1)));
        }

        navMenuIcons.recycle();

        return navDrawerItems;
    }
    protected void roatateDrawer(View mCustomView){
        imageButton = (ImageButton) mCustomView
                .findViewById(R.id.imageButton);
        imageButtonDrawer = (ImageButton) mCustomView
                .findViewById(R.id.menuButton);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mDrawerLayout =(DrawerLayout) findViewById(R.id.drawer_layout);
        imageButton.setRotation((float) 45.0);
        mDrawerLayout.openDrawer(Gravity.LEFT);
    }
    protected void resetDrawer(View mCustomView){
        imageButton = (ImageButton) mCustomView
                .findViewById(R.id.imageButton);
        imageButtonDrawer = (ImageButton) mCustomView
                .findViewById(R.id.menuButton);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mDrawerLayout =(DrawerLayout) findViewById(R.id.drawer_layout);
        imageButton.setRotation((float) 0.0);
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }
    public static String dateORTime(String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date date = new Date();
        return dateFormat.format(date).toString();
    }
    public void minimizeApp() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }
    public void showProgressBar(final Context context, final int iFlag){
        final Intent progressIntent = new Intent(getApplicationContext(), ProgressBar.class);
        startActivity(progressIntent);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent activityCall=null;
                if (iFlag==ONE){
                    activityCall = new Intent(context, WelcomeActivity.class);
                    activityCall.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(activityCall);
                }else if (iFlag==TWO){
                    activityCall = new Intent(context,HistoryList.class);
                    activityCall.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(activityCall);
                }else if (iFlag==THREE){
                    activityCall = new Intent(context,CustomizeReports.class);
                    activityCall.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(activityCall);
                }
            }
        }).start();
    }
}

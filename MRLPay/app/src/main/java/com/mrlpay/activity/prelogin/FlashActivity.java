package com.mrlpay.activity.prelogin;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.mrlpay.R;

/**
 * Created by Vignesh on 06-10-2016.
 */

public class FlashActivity extends AppCompatActivity {
    private BroadcastReceiver receiver = new ConnectionChangeReceiver();
    public static boolean connection=false;
    ConnectivityManager connectivityManager;
    NetworkInfo activeNetInfo;
    RelativeLayout relativeLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetInfo = connectivityManager.getActiveNetworkInfo();
        setContentView(R.layout.layout_flash);
        relativeLayout= (RelativeLayout) findViewById(R.id.footer);
        getSupportActionBar().hide();
        if (activeNetInfo!=null) {
            connection = true;
            relativeLayout.setVisibility(View.GONE);
        }else
            connection=false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if(receiver!= null){
                //unregisterReceiver(receiver);
                this.unregisterReceiver(this.receiver);
                receiver = null;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public class ConnectionChangeReceiver extends BroadcastReceiver {


        public ConnectionChangeReceiver() {
        }

        /* (non-Javadoc)
         * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
         */
        public void onReceive(Context context, Intent intent )
        {

            connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            activeNetInfo = connectivityManager.getActiveNetworkInfo();

            if(activeNetInfo == null){

            }else{

                if(connection) {
                    connection=false;
                    Intent mainIntent = new Intent(FlashActivity.this,
                            LoginActivity.class);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(mainIntent);
                }else{
                    connection=true;
                    Intent mainIntent = new Intent(FlashActivity.this,
                            LoginActivity.class);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(mainIntent);
                }
            }
        }
    }
}

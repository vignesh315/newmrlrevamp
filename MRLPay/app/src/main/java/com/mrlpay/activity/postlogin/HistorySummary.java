package com.mrlpay.activity.postlogin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mrlpay.MainActivity;
import com.mrlpay.R;
import com.mrlpay.adapter.Item;
import com.mrlpay.adapter.ItemData;
import com.mrlpay.adapter.ItemHistorySummaryAdapter;
import com.mrlpay.common.Util;

import java.util.ArrayList;

import static com.mrlpay.constants.Message.COLOR_NAVIG;
import static com.mrlpay.constants.Message.COMMA;
import static com.mrlpay.constants.Message.DOLLAR;
import static com.mrlpay.constants.Message.EMPTY;
import static com.mrlpay.constants.Message.FOUR;
import static com.mrlpay.constants.Message.FULLSTOP;
import static com.mrlpay.constants.Message.HEADING_HISTORY;
import static com.mrlpay.constants.Message.INITIAL;
import static com.mrlpay.constants.Message.ONE;
import static com.mrlpay.constants.Message.THREE;
import static com.mrlpay.constants.Message.TWO;

/**
 * Created by Vignesh on 09-12-2016.
 */

public class HistorySummary extends MainActivity{
    Activity activity;
    Typeface typeface;
    TextView headerText;
    ImageButton imageButton,imageButtonDrawer;
    Intent i = null;
    private String transIndexing[]={"",""};
    private int transIimageName[]={R.drawable.amex,Color.BLUE};
    private String transItemname[]={"Masetro Card","Receipt #5WTB"};
    private String transImageIdexing[]={"I","C"};
    private String transPrice[]={"999.00",""};
    private String transQuatity[]={"",""};

    private String indexing[]={"Ch","Sw","Sn","Pi","Bu","Ba","Ap"};
    private int imageName[]={Color.WHITE,Color.BLUE,Color.YELLOW,Color.GREEN,Color.RED,Color.CYAN,Color.MAGENTA};
    private String itemname[]={"Chocolate","Sweet","Snack","Pizza","Burger","Banana","Apple"};
    private String imageIdexing[]={"C","C","C","C","C","C","C"};
    private String price[]={"999.00","124.00","320.00","180.00","90.00","40.00","200.00"};
    private String quatity[]={"25","99","55","48","60","1","99"};

    private String totIndexing[]={"Di","Di","Sa","To"};
    private int totImageName[]={Color.GRAY,Color.GRAY,R.drawable.icon_currentbatch,Color.GRAY};
    private String totItemname[]={"Discount","Diwali","SaleTax(1%)","Total"};
    private String totImageIdexing[]={"C","C","I","C"};
    private String totPrice[]={"-$0.01","$0.00","included","0.00"};
    private String totQuatity[]={"","","",""};


    ArrayList refundDetails =  new ArrayList<>();
    ArrayList refundItemDetails =  new ArrayList<>();
    ArrayList refundItemTotal =  new ArrayList<>();

    //inflater layout for history summary
    RelativeLayout historySummaryLayout=null;

    //initialize the componente layout
    Button issueReceipt=null,issueRefund=null,issuereceipt=null;
    TextView transactionMode=null,transactiondate=null,item_label=null,total_label=null;
    EditText refundAmt=null;
    RecyclerView paymentsummary=null,itemsummary=null,itemtotaldetails=null;

    //Dialog for Refund receipt
    Dialog refundReceipt=null,refundAmount=null;

    RecyclerView.LayoutManager mLayoutManager;

    RadioButton returnGoods=null,accidentalCharge=null,cancelOrder=null,other=null;
    RadioGroup refundReasonSel=null;
    StringBuilder refundAmtBuilder=null;
    final int version = Build.VERSION.SDK_INT;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.historysummary);
        activity=this;
        typeface=getTypeface();
        View mCustomView;
        LayoutInflater mInflater = LayoutInflater.from(this);
        mCustomView = mInflater.inflate(R.layout.header_action_bar_leftarrow, null);
        android.support.v7.app.ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(COLOR_NAVIG)));
        headerText = (TextView) mCustomView.findViewById(R.id.headertext);
        headerText.setTypeface(typeface);
        imageButton = (ImageButton) mCustomView
                .findViewById(R.id.imageButton);
        imageButtonDrawer = (ImageButton) mCustomView
                .findViewById(R.id.menuButton);
        imageButton = (ImageButton) mCustomView
                .findViewById(R.id.imageButton);
        imageButtonDrawer = (ImageButton) mCustomView
                .findViewById(R.id.menuButton);
        headerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(activity,HistoryList.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
        //intializing inflated layout
        ItemHistoryPaymentDetails();
        //items add transaction details to the list
        itemHistorySummarryTransactionDetails();
        //item add details
        itemHistorySummaryItemAdded();
        //item amount details
        itemHistorySummaryItemTotal();
        //sale transaction summary details
        addingItemTransactionDetails();
        //sale item details
        addingItemItemDetails();
        //sale item total details
        addingItemItemTotalDetails();

        imageButtonDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getBaseContext(),"Please wait feching your history details",Toast.LENGTH_SHORT).show();
                showProgressBar(activity,TWO);
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //Toast.makeText(getBaseContext(),"Please wait feching your history details",Toast.LENGTH_SHORT).show();
                showProgressBar(activity,TWO);
            }
        });
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
    }
    private void itemHistorySummarryTransactionDetails(){
        refundDetails = new ArrayList<>();
        for (int i=0;i<transItemname.length;i++) {
            Item itemData = new Item(transIimageName[i],transImageIdexing[i], transIndexing[i], transItemname[i], transPrice[i], transQuatity[i]);
            refundDetails.add(itemData);
        }
    }
    private void itemHistorySummaryItemAdded(){
        refundItemDetails =  new ArrayList<>();
        for (int i=0;i<itemname.length;i++) {
            Item itemData = new Item(imageName[i],imageIdexing[i], indexing[i], itemname[i], price[i], quatity[i]);
            refundItemDetails.add(itemData);
        }
    }
    private  void itemHistorySummaryItemTotal(){
        refundItemTotal =  new ArrayList<>();
        for (int i=0;i<totItemname.length;i++) {
            Item itemData = new Item(totImageName[i],totImageIdexing[i],totIndexing[i],totItemname[i], totPrice[i], totQuatity[i]);
            refundItemTotal.add(itemData);
        }
    }
    private void ItemHistoryPaymentDetails(){
        refundReceipt=getFullScreenRefundReceiptPopupLayout();
        refundAmount=getFullScreenRefundRefundPopupLayout();
        issueReceipt = (Button)findViewById(R.id.issuereceipt);
        issueRefund = (Button) findViewById(R.id.issuerefund);
        issueReceipt.setTypeface(typeface);
        issueRefund.setTypeface(typeface);

        issueReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refundReceipt.show();
            }
        });
        issueRefund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refundAmtBuilder = new StringBuilder();
                refundAmt.setText(EMPTY);
                refundAmount.show();
            }
        });

        transactionMode = (TextView)findViewById(R.id.transactionMode);
        transactiondate = (TextView) findViewById(R.id.transactiondate);
        item_label = (TextView) findViewById(R.id.item_label);
        total_label = (TextView) findViewById(R.id.total_label);
        transactionMode.setTypeface(typeface);
        transactiondate.setTypeface(typeface);
        item_label.setTypeface(typeface);
        total_label.setTypeface(typeface);

        paymentsummary = (RecyclerView) findViewById(R.id.paymentsummary);
        itemsummary = (RecyclerView) findViewById(R.id.itemsummary);
        itemtotaldetails = (RecyclerView) findViewById(R.id.itemtotaldetails);
    }
    private void addingItemTransactionDetails(){
        ItemHistorySummaryAdapter itemHistorySummaryAdapter = new ItemHistorySummaryAdapter(refundDetails, typeface, new ItemHistorySummaryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Item item) {
            }
        });
        mLayoutManager = new LinearLayoutManager(activity);
        paymentsummary.setLayoutManager(mLayoutManager);
        paymentsummary.setItemAnimator(new DefaultItemAnimator());
        paymentsummary.setAdapter(itemHistorySummaryAdapter);
    }
    private void addingItemItemDetails(){
        ItemHistorySummaryAdapter itemHistorySummaryAdapter = new ItemHistorySummaryAdapter(refundItemDetails, typeface, new ItemHistorySummaryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Item item) {
            }
        });
        mLayoutManager = new LinearLayoutManager(activity);
        itemsummary.setLayoutManager(mLayoutManager);
        itemsummary.setItemAnimator(new DefaultItemAnimator());
        itemsummary.setAdapter(itemHistorySummaryAdapter);
    }
    private void addingItemItemTotalDetails(){
        ItemHistorySummaryAdapter itemHistorySummaryAdapter = new ItemHistorySummaryAdapter(refundItemTotal, typeface, new ItemHistorySummaryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Item item) {
            }
        });
        mLayoutManager = new LinearLayoutManager(activity);
        itemtotaldetails.setLayoutManager(mLayoutManager);
        itemtotaldetails.setItemAnimator(new DefaultItemAnimator());
        itemtotaldetails.setAdapter(itemHistorySummaryAdapter);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
    private Dialog getFullScreenRefundReceiptPopupLayout(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout customPopup = (LinearLayout) inflater.inflate(R.layout.refund_receipt_input_layout,null);
        TextView closeButton = (TextView) customPopup.findViewById(R.id.headerleftButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refundReceipt.dismiss();
            }
        });
        TextView headerText = (TextView) customPopup.findViewById(R.id.headerText);
        EditText emailID = (EditText)customPopup.findViewById(R.id.emailid);
        EditText phone = (EditText)customPopup.findViewById(R.id.phone);
        phone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    refundReceipt.dismiss();
                    Toast.makeText(getBaseContext(),"Please wait Email sent sucessfully",Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
        closeButton.setTypeface(typeface);
        headerText.setTypeface(typeface);
        emailID.setTypeface(typeface);
        phone.setTypeface(typeface);
        Dialog dialog=new Dialog(this,android.R.style.Theme_Black_NoTitleBar);
        dialog.setContentView(customPopup);
        return dialog;
    }
    private Dialog getFullScreenRefundRefundPopupLayout(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout customPopup = (LinearLayout) inflater.inflate(R.layout.refund_issue_refund_layout,null);
        TextView closeButton = (TextView) customPopup.findViewById(R.id.headerleftButton);
        TextView amttorefund = (TextView) customPopup.findViewById(R.id.amttorefund);
        TextView noterefundamt =(TextView) customPopup.findViewById(R.id.noterefundamt);
        TextView reason_label=(TextView) customPopup.findViewById(R.id.reason_label);

        refundReasonSel = (RadioGroup) customPopup.findViewById(R.id.refundreasonsel);
        returnGoods = (RadioButton) customPopup.findViewById(R.id.returngoods);
        accidentalCharge = (RadioButton) customPopup.findViewById(R.id.accidentalcharge);
        cancelOrder = (RadioButton) customPopup.findViewById(R.id.cancelorder);
        other = (RadioButton) customPopup.findViewById(R.id.other);

        issuereceipt = (Button)customPopup.findViewById(R.id.issuereceipt);


        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableRefundButton();
                refundAmount.dismiss();
            }
        });
        TextView headerText = (TextView) customPopup.findViewById(R.id.headerText);
        refundAmt = (EditText)customPopup.findViewById(R.id.refundamt);
        refundAmt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0) {
                    refundAmt.removeTextChangedListener(this);
                    String tempValue = s.subSequence(s.length() - 1, s.length()).toString();
                    refundAmtBuilder.append(tempValue);
                    refundAmt.setText(DOLLAR + Util.indianCurrencyFormat(refundAmtBuilder.toString()));
                    refundAmt.setSelection(refundAmt.getText().length());
                    refundAmt.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        refundAmt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    refundAmtBuilder = new StringBuilder();
                    refundAmt.setText(EMPTY);
                }
                return false;
            }
        });
        int selection=refundAmt.getText().length();
        refundAmt.setSelection(selection);
        refundAmt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    disableRefundButton();
                    refundAmount.dismiss();
                    Toast.makeText(getBaseContext(),"Please wait refunded sent sucessfully",Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
        closeButton.setTypeface(typeface);
        headerText.setTypeface(typeface);

        refundAmt.setTypeface(typeface);
        amttorefund.setTypeface(typeface);
        noterefundamt.setTypeface(typeface);
        reason_label.setTypeface(typeface);

        returnGoods.setTypeface(typeface);
        accidentalCharge.setTypeface(typeface);
        cancelOrder.setTypeface(typeface);
        other.setTypeface(typeface);

        issuereceipt.setTypeface(typeface);

        Dialog dialog=new Dialog(this,android.R.style.Theme_Black_NoTitleBar);
        dialog.setContentView(customPopup);
        return dialog;
    }
    public void onRadioButtonClicked(View view) {
        switch(view.getId()) {
            case R.id.returngoods:
                    selectRadioButtonOption(1);
                    break;
            case R.id.accidentalcharge:
                    selectRadioButtonOption(2);
                    break;
            case R.id.cancelorder:
                    selectRadioButtonOption(3);
                    break;
            case R.id.other:
                    selectRadioButtonOption(4);
                    break;
        }
    }
    private void selectRadioButtonOption(int option){
        if (ONE==option)
            returnGoods.setChecked(true);
        else
            returnGoods.setChecked(false);
        if (TWO==option)
            accidentalCharge.setChecked(true);
        else
            accidentalCharge.setChecked(false);
        if (THREE==option)
            cancelOrder.setChecked(true);
        else
            cancelOrder.setChecked(false);
        if (FOUR==option)
            other.setChecked(true);
        else
            other.setChecked(false);
        enableRefundButton();
    }
    private void enableRefundButton(){
        if (version >= 23)
            issuereceipt.setTextColor(ContextCompat.getColor(activity, R.color.buttoncolor));
         else
            issuereceipt.setTextColor(activity.getResources().getColor(R.color.buttoncolor));
        issuereceipt.setBackgroundResource(R.color.light_grey);
    }
    private  void disableRefundButton(){
        resetRadioButton();
        if (version >= 23)
            issuereceipt.setTextColor(ContextCompat.getColor(activity, R.color.light_grey));
        else
            issuereceipt.setTextColor(activity.getResources().getColor(R.color.light_grey));
        issuereceipt.setBackgroundResource(R.drawable.custom_button_style_border);
    }
    private void resetRadioButton(){
        returnGoods.setChecked(false);
        accidentalCharge.setChecked(false);
        cancelOrder.setChecked(false);
        other.setChecked(false);
    }
}

package com.mrlpay.activity.prelogin;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mrlpay.MainActivity;
import com.mrlpay.R;
import com.mrlpay.activity.postlogin.WelcomeActivity;
import com.mrlpay.common.ProgressBar;
import com.mrlpay.common.Util;

import static com.mrlpay.constants.Message.ONE;


/**
 * Created by Vignesh on 06-10-2016.
 */

public class LoginActivity extends MainActivity{
    Button btnLoginOk;
    int MyVersion;
    public static boolean session=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_login);
        hideActionBar();
        InitComponents();
        /*MyVersion = Build.VERSION.SDK_INT;
        if (!checkIfAlreadyhavePermission()
                && MyVersion > Build.VERSION_CODES.LOLLIPOP)
            requestForSpecificPermission();*/
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void InitComponents(){
        btnLoginOk = (Button) findViewById(R.id.btnLoginOk);
    }
    public void showSignIn(View view){
        session=true;
        //Toast.makeText(getBaseContext(),"Please wait we preparing your items to select",Toast.LENGTH_SHORT).show();
        showProgressBar(this,ONE);
        /*Intent intent = new Intent(LoginActivity.this, WelcomeActivity.class);
        startActivity(intent);*/
    }
}

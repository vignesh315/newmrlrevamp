package com.mrlpay.activity.postlogin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mrlpay.MainActivity;
import com.mrlpay.R;
import com.mrlpay.adapter.NavDrawerItem;
import com.mrlpay.adapter.NavDrawerListAdapter;
import com.mrlpay.common.Util;
import com.mrlpay.pageviewer.CustomPagerAdapter;

import java.util.ArrayList;


import static com.mrlpay.constants.Message.COLOR_NAVIG;
import static com.mrlpay.constants.Message.CURRENT_SALE;
import static com.mrlpay.constants.Message.EMPTY;
import static com.mrlpay.constants.Message.KEYPAD;
import static com.mrlpay.constants.Message.LIBRARY;
import static com.mrlpay.constants.Message.NO_SALE;
import static com.mrlpay.constants.Message.SAVE;
import static com.mrlpay.constants.Message.SET_PRICE;
import static com.mrlpay.constants.Message.THREE;
import static com.mrlpay.constants.Message.TWO;

/**
 * Created by Vignesh on 12-10-2016.
 */

public class WelcomeActivity extends MainActivity {
    ImageButton imageButtonDrawer;
    private DrawerLayout mDrawerLayout;
    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private String[] navMenuTitles={"welcome","transfer","billpay","logout"};
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter mAdapter;
    Activity activity;
    private ListView mDrawerList;
    public static boolean popup=false;
    ImageButton imageButton;
    TextView headerText,headerQuatityDis;
    TabLayout tabLayout ;
    TextView quickSaleAmtDisplay;
    Typeface typeface;
    View mCustomView;
    Button viewOrderButtonList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_screen);
        activity=this;
        //setting custom font
        typeface =getTypeface();
        // inflate header - starts
        LayoutInflater mInflater = LayoutInflater.from(this);
        mCustomView = mInflater.inflate(R.layout.header_action_bar, null);
        android.support.v7.app.ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(COLOR_NAVIG)));
        headerText = (TextView) mCustomView.findViewById(R.id.headertext);
        viewOrderButtonList =(Button) mCustomView.findViewById(R.id.viewOrderList);
        headerText.setTypeface(typeface);
        headerQuatityDis =(TextView)mCustomView.findViewById(R.id.quatitytext);
        headerQuatityDis.setTypeface(typeface);
        imageButton = (ImageButton) mCustomView
                .findViewById(R.id.imageButton);
        imageButtonDrawer = (ImageButton) mCustomView
                .findViewById(R.id.menuButton);
        // inflate header - ends
        moveDrawerToTop(R.layout.welcome_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mDrawerLayout =(DrawerLayout) findViewById(R.id.drawer_layout);
        setupDrawer();
        addDrawerItems();
        quickSaleAndLibrary();

        headerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    resetDrawer(mCustomView);
                }
            }
        });
        imageButtonDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        resetDrawer(mCustomView);
                        /*imageButton.setRotation((float) 0.0);
                        mDrawerLayout.closeDrawer(Gravity.LEFT);*/
                    } else {

                        if (popup == true) {
                            roatateDrawer(mCustomView);
                            /*imageButton.setRotation((float) 45.0);

                            mDrawerLayout.openDrawer(Gravity.LEFT);*/
                        } else {
                            roatateDrawer(mCustomView);
                            /*imageButton.setRotation((float) 45.0);

                            mDrawerLayout.openDrawer(Gravity.LEFT);*/
                        }
                    }
                }
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
    }
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Util.resetSharedPreference(getApplicationContext());
        resetHeader();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        startTimer();
        super.onStart();
    }
    private void setupDrawer() {
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                imageButtonDrawer.setRotation((float) 45.0);
                // actionBar.setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                //  textView.setText("Opened");
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                imageButtonDrawer.setRotation((float) 0.0);
                //  actionBar.setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                // textView.setText("Closed");
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }
    private void addDrawerItems() {

        mAdapter = new NavDrawerListAdapter(getApplicationContext(), getItems(),activity);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = null;
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        //Toast.makeText(getBaseContext(),"Please wait feching your history details",Toast.LENGTH_SHORT).show();
                        resetDrawer(mCustomView);
                        showProgressBar(activity,TWO);
                        break;
                    case 2:
                        //Toast.makeText(getBaseContext(),"Please wait feching your report details",Toast.LENGTH_SHORT).show();
                        resetDrawer(mCustomView);
                        showProgressBar(activity,THREE);
                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    case 5:

                        break;
                    case 6:

                        break;
                    case 7:

                        break;


                }
            }
        });
    }
    private void quickSaleAndLibrary(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout qsaleAndLibrary = (RelativeLayout) inflater.inflate(R.layout.quicksale_itemsale, null);
        final ViewPager viewPager = (ViewPager) qsaleAndLibrary.findViewById(R.id.viewpager);
        tabLayout  = (TabLayout) qsaleAndLibrary.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(KEYPAD));
        tabLayout.addTab(tabLayout.newTab().setText(LIBRARY));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorColor(Color.BLACK);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeface);
                }
            }
        }
        quickSaleAmtDisplay= (TextView) qsaleAndLibrary.findViewById(R.id.totalamount);
        final CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(this);
        customPagerAdapter.setmAppContext(getApplicationContext());
        customPagerAdapter.setDialog(getFullScreenPopupLayout());
        customPagerAdapter.setEditQuickSaleRate(getShowPopupLayoutEditQuickSaleRate());
        customPagerAdapter.setEditOrderListWithTax(getEditOrderListWithTax());
        customPagerAdapter.setListItemCartDialog(getListOfItemAdded());
        customPagerAdapter.setClearItemDialog(getshowPopupLayout());
        customPagerAdapter.setBtToViewOrderItem(viewOrderButtonList);
        customPagerAdapter.setHeaderText(headerText);
        customPagerAdapter.setHeaderQuatityDis(headerQuatityDis);
        customPagerAdapter.setQuickSaleAmtDisplay(quickSaleAmtDisplay);
        customPagerAdapter.setTypeface(typeface);
        viewPager.setAdapter(customPagerAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition()==0)
                    customPagerAdapter.resetSearchView();
                viewPager.setCurrentItem(tab.getPosition());
                customPagerAdapter.resetExpandableList();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.main_content);
        frameLayout.addView(qsaleAndLibrary);
    }
    private void resetHeader(){
        String headerValue = headerText.getText().toString();
        if (NO_SALE.equals(headerValue)) {
            headerText.setText(NO_SALE);
            headerQuatityDis.setVisibility(View.GONE);
        }else {
            headerText.setText(CURRENT_SALE);
            headerQuatityDis.setVisibility(View.GONE);
        }
    }
    private Dialog getEditOrderListWithTax(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout customPopup = (LinearLayout) inflater.inflate(R.layout.edit_item_from_ordered_list,null);
        TextView closeButton = (TextView) customPopup.findViewById(R.id.headerleftButton);
        TextView headerText = (TextView) customPopup.findViewById(R.id.headerText);
        Button rightButton = (Button)customPopup.findViewById(R.id.headerrightButton);
        EditText addNote = (EditText)customPopup.findViewById(R.id.addnote);
        TextView priceLabel = (TextView) customPopup.findViewById(R.id.pricelabel);
        EditText price = (EditText)customPopup.findViewById(R.id.price);
        TextView noteLabel = (TextView) customPopup.findViewById(R.id.notelabel);
        TextView subtract = (TextView) customPopup.findViewById(R.id.subtract);
        TextView quatity = (TextView) customPopup.findViewById(R.id.quatity);
        TextView taxLabel = (TextView) customPopup.findViewById(R.id.taxlabel);
        TextView saleTaxLabel = (TextView) customPopup.findViewById(R.id.saletaxlabel);
        Button removeITem =(Button)customPopup.findViewById(R.id.removeItem);
        priceLabel.setTypeface(typeface);
        price.setTypeface(typeface);
        noteLabel.setTypeface(typeface);
        subtract.setTypeface(typeface);
        closeButton.setTypeface(typeface);
        headerText.setTypeface(typeface);
        rightButton.setTypeface(typeface);
        addNote.setTypeface(typeface);
        quatity.setTypeface(typeface);
        taxLabel.setTypeface(typeface);
        saleTaxLabel.setTypeface(typeface);
        removeITem.setTypeface(typeface);
        Dialog dialog=new Dialog(this,android.R.style.Theme_Black_NoTitleBar);
        dialog.setContentView(customPopup);
        return dialog;
    }
    private Dialog getListOfItemAdded(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout customPopup = (RelativeLayout) inflater.inflate(R.layout.summary_list_item_ordered,null);
        TextView closeButton = (TextView) customPopup.findViewById(R.id.headerleftButton);
        TextView headerText = (TextView) customPopup.findViewById(R.id.headerText);
        closeButton.setTypeface(typeface);
        headerText.setTypeface(typeface);
        Dialog dialog=new Dialog(this,android.R.style.Theme_Black_NoTitleBar);
        dialog.setContentView(customPopup);
        return dialog;
    }
    private Dialog getFullScreenPopupLayout(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout customPopup = (LinearLayout) inflater.inflate(R.layout.custom_popup_over,null);
        TextView closeButton = (TextView) customPopup.findViewById(R.id.headerleftButton);
        TextView headerText = (TextView) customPopup.findViewById(R.id.headerText);
        Button rightButton = (Button)customPopup.findViewById(R.id.headerrightButton);
        EditText addNote = (EditText)customPopup.findViewById(R.id.addnote);
        closeButton.setTypeface(typeface);
        headerText.setTypeface(typeface);
        rightButton.setTypeface(typeface);
        addNote.setTypeface(typeface);
        Dialog dialog=new Dialog(this,android.R.style.Theme_Black_NoTitleBar);
        dialog.setContentView(customPopup);
        return dialog;
    }
    private  Dialog getshowPopupLayout(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout alertPopup = (RelativeLayout) inflater.inflate(R.layout.dialogwithtwobutton,null);
        TextView popupTitle = (TextView) alertPopup.findViewById(R.id.imageDialog);
        TextView popupBoby = (TextView) alertPopup.findViewById(R.id.textDialog);
        TextView popupNegativeButton = (TextView) alertPopup.findViewById(R.id.declineButton);
        TextView popupPositiveButton = (TextView) alertPopup.findViewById(R.id.acceptButton);
        popupTitle.setTypeface(typeface);
        popupBoby.setTypeface(typeface);
        popupNegativeButton.setTypeface(typeface);
        popupPositiveButton.setTypeface(typeface);
        Dialog dialog = new Dialog(this);
        dialog.setContentView(alertPopup);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }
    private  Dialog getShowPopupLayoutEditQuickSaleRate(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout cusPopupEditRate = (LinearLayout) inflater.inflate(R.layout.common_header_layout,null);
        RelativeLayout mainContent = (RelativeLayout) inflater.inflate(R.layout.calculator_layout,null);
        TextView addNoteTxtView = (TextView)mainContent.findViewById(R.id.note_text);
        TextView cal_Text = (TextView)mainContent.findViewById(R.id.cal_text);
        Button one=(Button)mainContent.findViewById(R.id.one);
        Button two=(Button)mainContent.findViewById(R.id.two);
        Button three=(Button)mainContent.findViewById(R.id.three);
        Button four=(Button)mainContent.findViewById(R.id.four);
        Button five=(Button)mainContent.findViewById(R.id.five);
        Button six=(Button)mainContent.findViewById(R.id.six);
        Button seven=(Button)mainContent.findViewById(R.id.seven);
        Button eight=(Button)mainContent.findViewById(R.id.eight);
        Button nine=(Button)mainContent.findViewById(R.id.nine);
        Button zero=(Button)mainContent.findViewById(R.id.zero);
        Button cancel=(Button)mainContent.findViewById(R.id.cancel);
        Button plus=(Button)mainContent.findViewById(R.id.plus);
        addNoteTxtView.setVisibility(View.GONE);
        cusPopupEditRate.addView(mainContent);
        TextView closeButton = (TextView) cusPopupEditRate.findViewById(R.id.headerleftButton);
        TextView headerText = (TextView) cusPopupEditRate.findViewById(R.id.headerText);
        headerText.setText(SET_PRICE);
        Button rightButton = (Button)cusPopupEditRate.findViewById(R.id.headerrightButton);
        rightButton.setText(SAVE);
        closeButton.setTypeface(typeface);
        headerText.setTypeface(typeface);
        rightButton.setTypeface(typeface);
        one.setTypeface(typeface);
        two.setTypeface(typeface);
        three.setTypeface(typeface);
        four.setTypeface(typeface);
        five.setTypeface(typeface);
        six.setTypeface(typeface);
        seven.setTypeface(typeface);
        eight.setTypeface(typeface);
        nine.setTypeface(typeface);
        zero.setTypeface(typeface);
        cancel.setTypeface(typeface);
        plus.setText(EMPTY);
        plus.setTextColor(Color.WHITE);
        plus.setBackgroundColor(Color.WHITE);
        plus.setEnabled(false);
        cal_Text.setTypeface(typeface);
        Dialog dialog=new Dialog(this,android.R.style.Theme_Black_NoTitleBar);
        dialog.setContentView(cusPopupEditRate);
        return dialog;
    }
}

package com.mrlpay.activity.postlogin;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.mrlpay.MainActivity;
import com.mrlpay.R;

import java.util.Calendar;

import static com.mrlpay.constants.Message.DATEPICKERDIALOG;
import static com.mrlpay.constants.Message.END_DATE_SEL;
import static com.mrlpay.constants.Message.END_TIME_SEL;
import static com.mrlpay.constants.Message.START_DATE_SEL;
import static com.mrlpay.constants.Message.START_TIME_SEL;
import static com.mrlpay.constants.Message.ST_ZERO;
import static com.mrlpay.constants.Message.TEN;
import static com.mrlpay.constants.Message.TIMEPICKERDIALOG;

/**
 * Created by Vignesh on 29-12-2016.
 */

public class ReportRangeSelect extends MainActivity implements
        DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener{
    Activity activity;
    Typeface typeface;
    TextView headerText;
    Calendar instance1,instance2;
    SwitchCompat switchCompat;
    RelativeLayout timeSelectionLayout;
    //select date
    TextView startDate,endDate,startTime,endTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_calenderview_date_range_sel);
        hideActionBar();
        activity=this;
        typeface=getTypeface();
        customizeDialogView();
        resetTimeSelection();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void customizeDialogView(){
        instance1 = Calendar.getInstance();
        instance1.set(instance1.get(Calendar.YEAR), Calendar.JANUARY, 1);

        instance2 = Calendar.getInstance();
        instance2.set(instance2.get(Calendar.YEAR), Calendar.DECEMBER, 31);


        TextView closeButton = (TextView)findViewById(R.id.headerleftButton);
        TextView headerText = (TextView)findViewById(R.id.headerText);
        Button rightButton = (Button)findViewById(R.id.headerrightButton);

        startDate = (TextView) findViewById(R.id.startDate);
        endDate = (TextView) findViewById(R.id.endtDate);

        TextView selAllDates = (TextView)findViewById(R.id.sel_aldates_label);

        startTime = (TextView)findViewById(R.id.startTime);
        endTime = (TextView)findViewById(R.id.endTime);

        timeSelectionLayout = (RelativeLayout)findViewById(R.id.timesel_layout);

        switchCompat=(SwitchCompat)findViewById(R.id.switch_compat);

        ImageView selectDates = (ImageView)findViewById(R.id.selectDates);

        Button seletTime=(Button)findViewById(R.id.selectTime);
        seletTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTimeSelectionEnable();
            }
        });

        selectDates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        (DatePickerDialog.OnDateSetListener) activity,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAutoHighlight(true);
                dpd.show(getFragmentManager(),DATEPICKERDIALOG);
            }
        });

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    setTimeSelectionDisable();
                else
                    setTimeSelectionEnable();
            }
        });

        closeButton.setTypeface(typeface);
        headerText.setTypeface(typeface);
        rightButton.setTypeface(typeface);

        startDate.setTypeface(typeface);
        endDate.setTypeface(typeface);
        selAllDates.setTypeface(typeface);
        startTime.setTypeface(typeface);
        endTime.setTypeface(typeface);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callCustomizeReportActivity();
            }
        });
    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        //String date = "You picked the following date: From- "+dayOfMonth+"/"+(++monthOfYear)+"/"+year+" To "+dayOfMonthEnd+"/"+(++monthOfYearEnd)+"/"+yearEnd;
        String sDate=dayOfMonth+"/"+(++monthOfYear)+"/"+year;
        String sEnd =dayOfMonthEnd+"/"+(++monthOfYearEnd)+"/"+yearEnd;
        startDate.setText(sDate);
        endDate.setText(sEnd);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {
        String hourString = hourOfDay < TEN ? ST_ZERO+hourOfDay : ""+hourOfDay;
        String minuteString = minute < TEN ? ST_ZERO+minute : ""+minute;
        String hourStringEnd = hourOfDayEnd < TEN ? ST_ZERO+hourOfDayEnd : ""+hourOfDayEnd;
        String minuteStringEnd = minuteEnd < TEN ? ST_ZERO+minuteEnd : ""+minuteEnd;
        //String time = "You picked the following time: From - "+hourString+"h"+minuteString+" To - "+hourStringEnd+"h"+minuteStringEnd;
        String sTime=hourString+" : "+minuteString;
        String eTime=hourStringEnd+" : "+minuteStringEnd;
        startTime.setText(sTime);
        endTime.setText(eTime);
    }
    private void setTimeSelectionEnable(){
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                (TimePickerDialog.OnTimeSetListener) activity,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.show(getFragmentManager(), TIMEPICKERDIALOG);
        timeSelectionLayout.setVisibility(View.VISIBLE);
    }
    private void setTimeSelectionDisable(){
        timeSelectionLayout.setVisibility(View.GONE);
    }
    private void resetTimeSelection(){
        startDate.setText(START_DATE_SEL);
        endDate.setText(END_DATE_SEL);
        startTime.setText(START_TIME_SEL);
        endTime.setText(END_TIME_SEL);
        switchCompat.setChecked(true);
        timeSelectionLayout.setVisibility(View.GONE);
    }
    private void callCustomizeReportActivity(){
        Intent intent = new Intent(activity,CustomizeReports.class);
        startActivity(intent);
    }
 }

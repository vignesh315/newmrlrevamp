package com.mrlpay.activity.postlogin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.SwitchCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mrlpay.MainActivity;
import com.mrlpay.R;
import com.mrlpay.adapter.NavDrawerListAdapter;

import java.util.Calendar;

import static com.mrlpay.constants.Message.COLOR_NAVIG;
import static com.mrlpay.constants.Message.HEADING_REPORTS;
import static com.mrlpay.constants.Message.ONE;
import static com.mrlpay.constants.Message.TWO;

/**
 * Created by Vignesh on 23-12-2016.
 */

public class CustomizeReports extends MainActivity{
    Activity activity;
    Typeface typeface;
    TextView headerText;
    ImageButton imageButton,imageButtonDrawer;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private NavDrawerListAdapter mAdapter;
    ImageView selectDateOption =null;
    Intent i = null;
    View mCustomView;
    public static boolean popup=false;

    //select date
    TextView startDate,endDate,startTime,endTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_screen);
        activity=this;
        typeface=getTypeface();
        LayoutInflater mInflater = LayoutInflater.from(this);
        mCustomView = mInflater.inflate(R.layout.header_action_bar_with_mail_date_sel, null);
        android.support.v7.app.ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(COLOR_NAVIG)));
        headerText = (TextView) mCustomView.findViewById(R.id.headertext);
        headerText.setText(HEADING_REPORTS);
        headerText.setTypeface(typeface);
        //added this line to select custom calender
        selectDateOption= (ImageView) mCustomView.findViewById(R.id.date_sel);
        selectDateOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callReportSelectActivity();
            }
        });
        imageButton = (ImageButton) mCustomView
                .findViewById(R.id.imageButton);
        imageButtonDrawer = (ImageButton) mCustomView
                .findViewById(R.id.menuButton);
        imageButton = (ImageButton) mCustomView
                .findViewById(R.id.imageButton);
        imageButtonDrawer = (ImageButton) mCustomView
                .findViewById(R.id.menuButton);
        // inflate header - ends
        moveDrawerToTop(R.layout.welcome_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mDrawerLayout =(DrawerLayout) findViewById(R.id.drawer_layout);
        setupDrawer();
        addDrawerItems();
        headerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    resetDrawer(mCustomView);
                }
            }
        });
        imageButtonDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        resetDrawer(mCustomView);
                        /*imageButton.setRotation((float) 0.0);
                        mDrawerLayout.closeDrawer(Gravity.LEFT);*/
                    } else {

                        if (popup == true) {
                            roatateDrawer(mCustomView);
                            /*imageButton.setRotation((float) 45.0);

                            mDrawerLayout.openDrawer(Gravity.LEFT);*/
                        } else {
                            roatateDrawer(mCustomView);
                            /*imageButton.setRotation((float) 45.0);

                            mDrawerLayout.openDrawer(Gravity.LEFT);*/
                        }
                    }
                }
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    private void setupDrawer() {
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                imageButtonDrawer.setRotation((float) 45.0);
                // actionBar.setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                //  textView.setText("Opened");
            }

            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                imageButtonDrawer.setRotation((float) 0.0);
                //  actionBar.setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                // textView.setText("Closed");
            }
        };
    }
    private void addDrawerItems() {

        mAdapter = new NavDrawerListAdapter(getApplicationContext(), getItems(), activity);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = null;
                switch (position) {
                    case 0:
                        //Toast.makeText(getBaseContext(),"Please wait we preparing your items to select",Toast.LENGTH_SHORT).show();
                        resetDrawer(mCustomView);
                        showProgressBar(activity,ONE);
                        break;
                    case 1:
                        //Toast.makeText(getBaseContext(),"Please wait feching your history details",Toast.LENGTH_SHORT).show();
                        resetDrawer(mCustomView);
                        showProgressBar(activity,TWO);
                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    case 5:

                        break;
                    case 6:

                        break;
                    case 7:

                        break;


                }
            }
        });
    }
    private void callReportSelectActivity(){
        Intent intent = new Intent(activity,ReportRangeSelect.class);
        startActivity(intent);
    }
}
package com.mrlpay.activity.postlogin;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mrlpay.MainActivity;
import com.mrlpay.R;
import com.mrlpay.adapter.NavDrawerListAdapter;
import com.mrlpay.adapter.StickyHeaderListAdapter;

import java.util.WeakHashMap;

import se.emilsjolander.stickylistheaders.ExpandableStickyListHeadersListView;

import static com.mrlpay.constants.Message.COLOR_NAVIG;
import static com.mrlpay.constants.Message.HEADING_HISTORY;
import static com.mrlpay.constants.Message.ONE;
import static com.mrlpay.constants.Message.THREE;

/**
 * Created by Vignesh on 02-12-2016.
 */

public class HistoryList extends MainActivity{
    Activity activity;
    Typeface typeface;
    TextView headerText;
    ImageButton imageButton,imageButtonDrawer;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private NavDrawerListAdapter mAdapter;
    public static boolean popup=false;
    View mCustomView;
    private final Context mcontext;
    //added variable to sticky expandable list
    private ExpandableStickyListHeadersListView mListView;
    WeakHashMap<View,Integer> mOriginalViewHeightPool = new WeakHashMap<View, Integer>();
    TextView itemSummary,itemCustomName,itemTime;

    public HistoryList() {
        mcontext = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_screen);
        activity=this;
        //setting custom font
        typeface = Typeface.createFromAsset(getAssets(),"fonts/calibri.otf");
        // inflate header - starts
        LayoutInflater mInflater = LayoutInflater.from(this);
        mCustomView = mInflater.inflate(R.layout.header_action_bar_leftside_title, null);
        android.support.v7.app.ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(COLOR_NAVIG)));
        headerText = (TextView) mCustomView.findViewById(R.id.headertext);
        headerText.setText(HEADING_HISTORY);
        headerText.setTypeface(typeface);
        imageButton = (ImageButton) mCustomView
                .findViewById(R.id.imageButton);
        imageButtonDrawer = (ImageButton) mCustomView
                .findViewById(R.id.menuButton);
        imageButton = (ImageButton) mCustomView
                .findViewById(R.id.imageButton);
        imageButtonDrawer = (ImageButton) mCustomView
                .findViewById(R.id.menuButton);
        // inflate header - ends
        moveDrawerToTop(R.layout.welcome_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        mDrawerLayout =(DrawerLayout) findViewById(R.id.drawer_layout);
        setupDrawer();
        addDrawerItems();
        stickyHeaderListAddToFrameLayout();
        headerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    resetDrawer(mCustomView);
                }
            }
        });
        imageButtonDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        resetDrawer(mCustomView);
                        /*imageButton.setRotation((float) 0.0);
                        mDrawerLayout.closeDrawer(Gravity.LEFT);*/
                    } else {

                        if (popup == true) {
                            roatateDrawer(mCustomView);
                            /*imageButton.setRotation((float) 45.0);

                            mDrawerLayout.openDrawer(Gravity.LEFT);*/
                        } else {
                            roatateDrawer(mCustomView);
                            /*imageButton.setRotation((float) 45.0);

                            mDrawerLayout.openDrawer(Gravity.LEFT);*/
                        }
                    }
                }
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
    }
    private void setupDrawer() {
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                imageButtonDrawer.setRotation((float) 45.0);
                // actionBar.setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                //  textView.setText("Opened");
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                imageButtonDrawer.setRotation((float) 0.0);
                //  actionBar.setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                // textView.setText("Closed");
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }
    private void addDrawerItems() {

        mAdapter = new NavDrawerListAdapter(getApplicationContext(), getItems(), activity);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = null;
                switch (position) {
                    case 0:
                        //Toast.makeText(getBaseContext(),"Please wait we preparing your items to select",Toast.LENGTH_SHORT).show();
                        resetDrawer(mCustomView);
                        showProgressBar(activity,ONE);
                        break;
                    case 1:
                        break;
                    case 2:
                        resetDrawer(mCustomView);
                        showProgressBar(activity,THREE);
                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    case 5:

                        break;
                    case 6:

                        break;
                    case 7:

                        break;


                }
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void stickyHeaderListAddToFrameLayout(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout historySummary = (LinearLayout) inflater.inflate(R.layout.sticky_header_list_exapandable_layout, null);
        mListView = (ExpandableStickyListHeadersListView) historySummary.findViewById(R.id.list);
        mListView.setAnimExecutor(new AnimationExecutor());
        StickyHeaderListAdapter stickyHeaderListAdapter = new StickyHeaderListAdapter(this,getTypeface());
        mListView.setAdapter(stickyHeaderListAdapter);
       /* commented this this line to no make any action onclick of header
       mListView.setOnHeaderClickListener(new StickyListHeadersListView.OnHeaderClickListener() {
            @Override
            public void onHeaderClick(StickyListHeadersListView stickyListHeadersListView, View view, int itemPosition, long headerId, boolean b) {
                if(mListView.isHeaderCollapsed(headerId)){
                    mListView.expand(headerId);
                }else {
                    mListView.collapse(headerId);
                }
            }
        });*/
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemSummary = (TextView) view.findViewById(R.id.amount);
                itemCustomName = (TextView) view.findViewById(R.id.itemname);
                itemTime = (TextView) view.findViewById(R.id.time);
                Toast.makeText(getBaseContext(),"Please wait feching your history Summary details",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(activity,HistorySummary.class);
                startActivity(i);
            }
        });
        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.main_content);
        frameLayout.addView(historySummary);
    }
    //animation executor
    class AnimationExecutor implements ExpandableStickyListHeadersListView.IAnimationExecutor {

        @Override
        public void executeAnim(final View target, final int animType) {
            if(ExpandableStickyListHeadersListView.ANIMATION_EXPAND==animType&&target.getVisibility()==View.VISIBLE){
                return;
            }
            if(ExpandableStickyListHeadersListView.ANIMATION_COLLAPSE==animType&&target.getVisibility()!=View.VISIBLE){
                return;
            }
            if(mOriginalViewHeightPool.get(target)==null){
                mOriginalViewHeightPool.put(target,target.getHeight());
            }
            final int viewHeight = mOriginalViewHeightPool.get(target);
            float animStartY = animType == ExpandableStickyListHeadersListView.ANIMATION_EXPAND ? 0f : viewHeight;
            float animEndY = animType == ExpandableStickyListHeadersListView.ANIMATION_EXPAND ? viewHeight : 0f;
            final ViewGroup.LayoutParams lp = target.getLayoutParams();
            ValueAnimator animator = ValueAnimator.ofFloat(animStartY, animEndY);
            animator.setDuration(200);
            target.setVisibility(View.VISIBLE);
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    if (animType == ExpandableStickyListHeadersListView.ANIMATION_EXPAND) {
                        target.setVisibility(View.VISIBLE);
                    } else {
                        target.setVisibility(View.GONE);
                    }
                    target.getLayoutParams().height = viewHeight;
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    lp.height = ((Float) valueAnimator.getAnimatedValue()).intValue();
                    target.setLayoutParams(lp);
                    target.requestLayout();
                }
            });
            animator.start();

        }
    }
}

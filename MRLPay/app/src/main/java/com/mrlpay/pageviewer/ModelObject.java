package com.mrlpay.pageviewer;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;

import com.mrlpay.R;
import com.mrlpay.adapter.Item;
import com.mrlpay.adapter.ItemListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vignesh on 17-10-2016.
 */

public enum ModelObject {

    KEYPAD(R.string.keypad, R.layout.calculator_layout),
    LIBRARY(R.string.library, R.layout.library_sale);
    private int mTitleResId;
    private int mLayoutResId;

    ModelObject(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }
}

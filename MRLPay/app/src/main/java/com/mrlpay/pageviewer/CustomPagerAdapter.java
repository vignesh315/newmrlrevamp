package com.mrlpay.pageviewer;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mrlpay.R;
import com.mrlpay.adapter.Item;
import com.mrlpay.adapter.ItemAddToCartAdapter;
import com.mrlpay.adapter.ItemData;
import com.mrlpay.adapter.ItemListAdapter;
import com.mrlpay.common.Util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.mrlpay.constants.Message.ADD_NOTE;
import static com.mrlpay.constants.Message.ALL_ITEMS;
import static com.mrlpay.constants.Message.ALTER_ITEM_PRICE_SUM;
import static com.mrlpay.constants.Message.ALTER_ITEM_QUATITY_SUM;
import static com.mrlpay.constants.Message.CHARGE;
import static com.mrlpay.constants.Message.CLEAR_ALL;
import static com.mrlpay.constants.Message.COMMA;
import static com.mrlpay.constants.Message.CURRENT_ITEM_POSITION;
import static com.mrlpay.constants.Message.CURRENT_ITEM_SALE_TYPE;
import static com.mrlpay.constants.Message.CURRENT_SALE;
import static com.mrlpay.constants.Message.CUSTOM_AMOUNT;
import static com.mrlpay.constants.Message.DEFAULTCURRENCYFORMAT;
import static com.mrlpay.constants.Message.DISCOUNT;
import static com.mrlpay.constants.Message.DISCOUNT_ON_SALE;
import static com.mrlpay.constants.Message.DOLLAR;
import static com.mrlpay.constants.Message.D_ZERO;
import static com.mrlpay.constants.Message.ELEVEN;
import static com.mrlpay.constants.Message.EMPTY;
import static com.mrlpay.constants.Message.FINALAMT;
import static com.mrlpay.constants.Message.HUNDRED;
import static com.mrlpay.constants.Message.INDIANCURREFORMATONE;
import static com.mrlpay.constants.Message.INDIANCURRFORMAT;
import static com.mrlpay.constants.Message.INITIAL;
import static com.mrlpay.constants.Message.IS_ITEM_TAXABLE;
import static com.mrlpay.constants.Message.ITEM_AMOUNT;
import static com.mrlpay.constants.Message.ITEM_COUNT;
import static com.mrlpay.constants.Message.ITEM_CUSTOM_NOTE;
import static com.mrlpay.constants.Message.ITEM_DISCOUNT;
import static com.mrlpay.constants.Message.ITEM_DISCOUNT_AMT;
import static com.mrlpay.constants.Message.ITEM_NAME;
import static com.mrlpay.constants.Message.ITEM_ORDER_TAG;
import static com.mrlpay.constants.Message.ITEM_QUATITY;
import static com.mrlpay.constants.Message.ITEM_SALE_TYPE;
import static com.mrlpay.constants.Message.ITEM_TAXABLE;
import static com.mrlpay.constants.Message.ITEM_TOTAL_AMT;
import static com.mrlpay.constants.Message.LIBRARY_SALE;
import static com.mrlpay.constants.Message.MINUS;
import static com.mrlpay.constants.Message.NOT_TAXABLE;
import static com.mrlpay.constants.Message.NO_DATA_AVAIL;
import static com.mrlpay.constants.Message.NO_SALE;
import static com.mrlpay.constants.Message.NULL;
import static com.mrlpay.constants.Message.ONE;
import static com.mrlpay.constants.Message.ONE_LINE_SPACE;
import static com.mrlpay.constants.Message.QUICK_SALE;
import static com.mrlpay.constants.Message.SELECTED_ITEM_NAME;
import static com.mrlpay.constants.Message.SEVEN;
import static com.mrlpay.constants.Message.SINGLE_ITEM_TAX_CHK;
import static com.mrlpay.constants.Message.ST_EIGHT;
import static com.mrlpay.constants.Message.ST_FIVE;
import static com.mrlpay.constants.Message.ST_FOUR;
import static com.mrlpay.constants.Message.ST_NINE;
import static com.mrlpay.constants.Message.ST_ONE;
import static com.mrlpay.constants.Message.ST_SEVEN;
import static com.mrlpay.constants.Message.ST_SIX;
import static com.mrlpay.constants.Message.ST_THREE;
import static com.mrlpay.constants.Message.ST_TWO;
import static com.mrlpay.constants.Message.ST_ZERO;
import static com.mrlpay.constants.Message.TAXABLE;
import static com.mrlpay.constants.Message.TILDA;
import static com.mrlpay.constants.Message.TILDE;
import static com.mrlpay.constants.Message.TOTAL_DISCOUNT_ON_ITEM;
import static com.mrlpay.constants.Message.TRANSLUCENT;
import static com.mrlpay.constants.Message.TWO;
import static com.mrlpay.constants.Message.ZERO;

/**
 * Created by Vignesh on 06/10/16.
 */
public class CustomPagerAdapter extends PagerAdapter implements View.OnClickListener {

    private Context mContext,mAppContext;

    private List<Item> itemList = new ArrayList<>();
    private ItemListAdapter itemListAdapter;
    private ItemAddToCartAdapter itemAddToCartAdapter;
    private int imageName[]={Color.WHITE,Color.BLUE,Color.YELLOW,Color.GREEN,Color.RED,Color.CYAN,Color.MAGENTA};
    private String indexing[]={"Ch","Sw","Sn","Pi","Bu","Ba","Ap"};
    private String itemname[]={"Chocolate","Sweet","Snack","Pizza","Burger","Banana","Apple"};
    private String imageIdexing[]={"C","C","C","C","C","C","C"};
    private String price[]={"999.00","124.00","320.00","180.00","90.00","40.00","200.00"};
    private String quatity[]={"25","99","55","48","60","1","99"};

    private int disImageName[]={Color.GRAY};
    private String disIndexing[]={""};
    private String disItemname[]={"DisCount"};
    private String disPrice[]={"-0.12"};
    private String disQuatity[]={"0"};
    RecyclerView recyclerView=null,orderedRecyclerView;
    SearchView search;
    ViewGroup layout;
    TextView quickSaleAmount,quickSaleTag,edqSaleRateCalText;
    Button one,two,three,four,five,six,seven,eight,nine,zero,cancel,plus;
    Button edOne,edTwo,edThree,edFour,edFive,edSix,edSeven,edEight,edNine,edZero,edCancel;
    SearchView.OnQueryTextListener listener;
    StringBuilder stringBuilder,finalBuilder,qSaleSingleRateEdit;
    private TextView headerText,headerQuatityDis,quickSaleAmtDisplay;
    TextView headerTxt,headerQuatity,quickSaleAmt;
    TextView popupCloseButton,popAddButton,pCancelItem,pClearItem,popupClearButton,headerTextClearOption,headerTxtItemInCard;
    TextView pCloseBtOrderList,pSubBtOrderRemove,pAddBtOrderList,pOrderListHeader;
    TextView pCloseBtEditQSaleRate;
    Button pSaveBtOrderList,pSaveBtEditQSaleRate,removeSeletedItem,confrimAndRemoveItem,btToViewOrderItem,btAdapViewOrderItem;//newly added for proper touch implementation btToViewOrderItem
    EditText addNoteText,edOrderPrice,edOrderNoteAndQuality,edQuatity;
    LinearLayout price_Layout,price_Label_Layout,label_Tax_Layout;
    private Dialog dialog,clearItemDialog,listItemCartDialog,editOrderListWithTax,editQuickSaleRate;
    Dialog dialogItem,clearDialog,listCartDialog,edOrderListWithTax,edQuickSaleRate;
    int quat;
    ExpandableListView expandableListView,clearExpandableListView;
    CustomExpandableListAdapter expandableListAdapter,clearExpandableListAdpater;
    List<String> expandableListTitle,clearexpanbleListTitle;
    HashMap<String, List<String>> expandableListDetail,clearExpandList;
    RelativeLayout expListLayout,expListClearLayout,taxableLayoutOrderList;
    RelativeLayout.LayoutParams LayoutExpand;
    RelativeLayout.LayoutParams LayoutCollapse;
    RecyclerView.LayoutManager mLayoutManager;
    SwitchCompat switchCompat;
    Double totalSumAmount=0.0;
    private boolean isClearExpanded=false;
    private Typeface typeface;
    protected Typeface setTypeface;
    static double itPrice=0.0;
    static int itQuatity=0;
    static String itSaleType=null;
    static String itSaleNote=EMPTY;
    static String itSaleItemTaxable=EMPTY;
    static String finalItemName=null;
    List<ItemData> itemOrderList = new ArrayList<>();
    ArrayList<String> fItemName=new ArrayList<String>();
    ArrayList<String> fItemCustomNote=new ArrayList<String>();
    ArrayList<String> fItemPrice=new ArrayList<String>();
    ArrayList<String>  fItemQty=new ArrayList<String>();
    ArrayList<String>  fSaleType=new ArrayList<String>();
    ArrayList<String>  fSaleItemTaxable=new ArrayList<String>();
    public CustomPagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        ModelObject modelObject = ModelObject.values()[position];
        setTypeface=getTypeface();
        LayoutExpand = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        LayoutCollapse = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        layout = (ViewGroup) inflater.inflate(modelObject.getLayoutResId(), collection, false);
        //edit Quick Sale rate
        edQuickSaleRate=getEditQuickSaleRate();
        qSaleSingleRateEdit=new StringBuilder();
        edOne= (Button) edQuickSaleRate.findViewById(R.id.one);
        edTwo=(Button)edQuickSaleRate.findViewById(R.id.two);
        edThree=(Button)edQuickSaleRate.findViewById(R.id.three);
        edFour=(Button)edQuickSaleRate.findViewById(R.id.four);
        edFive=(Button)edQuickSaleRate.findViewById(R.id.five);
        edSix=(Button)edQuickSaleRate.findViewById(R.id.six);
        edSeven=(Button)edQuickSaleRate.findViewById(R.id.seven);
        edEight=(Button)edQuickSaleRate.findViewById(R.id.eight);
        edNine=(Button)edQuickSaleRate.findViewById(R.id.nine);
        edZero=(Button)edQuickSaleRate.findViewById(R.id.zero);
        edCancel=(Button)edQuickSaleRate.findViewById(R.id.cancel);
        edqSaleRateCalText=(TextView)edQuickSaleRate.findViewById(R.id.cal_text);
        pCloseBtEditQSaleRate = (TextView) edQuickSaleRate.findViewById(R.id.headerleftButton);
        pSaveBtEditQSaleRate =(Button) edQuickSaleRate.findViewById(R.id.headerrightButton);
        pCloseBtEditQSaleRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qSaleSingleRateEdit=new StringBuilder();
                edqSaleRateCalText.setText(INITIAL);
                edQuickSaleRate.dismiss();
            }
        });
        //set onclick Listener
        edOne.setOnClickListener(quickSaleRateEdit);
        edTwo.setOnClickListener(quickSaleRateEdit);
        edThree.setOnClickListener(quickSaleRateEdit);
        edFour.setOnClickListener(quickSaleRateEdit);
        edFive.setOnClickListener(quickSaleRateEdit);
        edSix.setOnClickListener(quickSaleRateEdit);
        edSeven.setOnClickListener(quickSaleRateEdit);
        edEight.setOnClickListener(quickSaleRateEdit);
        edNine.setOnClickListener(quickSaleRateEdit);
        edZero.setOnClickListener(quickSaleRateEdit);
        edCancel.setOnClickListener(quickSaleRateEdit);

        //edit order dialog item
        edOrderListWithTax=getEditOrderListWithTax();
        pCloseBtOrderList = (TextView)edOrderListWithTax.findViewById(R.id.headerleftButton);
        pSaveBtOrderList = (Button)edOrderListWithTax.findViewById(R.id.headerrightButton);
        pSubBtOrderRemove = (TextView) edOrderListWithTax.findViewById(R.id.subtract);
        pOrderListHeader = (TextView)edOrderListWithTax.findViewById(R.id.headerText);
        pAddBtOrderList = (TextView) edOrderListWithTax.findViewById(R.id.addquantity);
        edOrderPrice=(EditText) edOrderListWithTax.findViewById(R.id.price);
        removeSeletedItem= (Button) edOrderListWithTax.findViewById(R.id.removeItem);
        confrimAndRemoveItem=(Button) edOrderListWithTax.findViewById(R.id.confremoveItem);
        removeSeletedItem.setTypeface(setTypeface);
        confrimAndRemoveItem.setTypeface(setTypeface);
        removeSeletedItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confrimAndRemoveItem.setVisibility(View.VISIBLE);
                removeSeletedItem.setVisibility(View.GONE);
            }
        });
        confrimAndRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edQuatity.setText(ST_ZERO);
                afterEditofItemCheckOnItemList();
            }
        });
        edOrderPrice.setFocusable(false);
        edOrderPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edqSaleRateCalText.setText(edOrderPrice.getText().toString());
                edQuickSaleRate.show();
            }
        });
        edOrderPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editItemQuickSaleRateSingleItem();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //addeded to edit rate for single item
        pSaveBtEditQSaleRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qSaleSingleRateEdit=new StringBuilder();
                String getEditedRate =edqSaleRateCalText.getText().toString();
                if (INITIAL.equals(getEditedRate))
                    pSaveBtOrderList.setEnabled(false);
                else
                    pSaveBtOrderList.setEnabled(true);
                    edOrderPrice.setText(getEditedRate);
                    edqSaleRateCalText.setText(INITIAL);
                edQuickSaleRate.dismiss();
            }
        });
        edOrderNoteAndQuality=(EditText) edOrderListWithTax.findViewById(R.id.addnote);
        edQuatity=(EditText) edOrderListWithTax.findViewById(R.id.quatity);
        price_Layout =(LinearLayout)edOrderListWithTax.findViewById(R.id.price_layout);
        price_Label_Layout = (LinearLayout)edOrderListWithTax.findViewById(R.id.pricelabel_layout);
        label_Tax_Layout=(LinearLayout)edOrderListWithTax.findViewById(R.id.label_tax_layout);
        taxableLayoutOrderList=(RelativeLayout)edOrderListWithTax.findViewById(R.id.taxable_layout);
        switchCompat=(SwitchCompat)edOrderListWithTax.findViewById(R.id.switch_compat);

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Util.setData_Boolean(SINGLE_ITEM_TAX_CHK,isChecked,getmAppContext());
            }
        });
        //editor on text change amount value should be recalculated.
        edQuatity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editItemAddOrRemoveQuatityManually();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //edit add order to remove the number of items exceeded
        pSubBtOrderRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editItemRemoveQuatity();
            }
        });
        //edit add order to add the quatity to the item ordered
        pAddBtOrderList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editItemAddQuatity();
            }
        });
        pSaveBtOrderList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //need to be added in method here
                afterEditofItemCheckOnItemList();
            }
        });
        pCloseBtOrderList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edOrderListWithTax.dismiss();
            }
        });
        //add to card full dialog list
        listCartDialog=getListItemCartDialog();
        popupClearButton = (TextView) listCartDialog.findViewById(R.id.headerleftButton);
        headerTextClearOption = (TextView)listCartDialog.findViewById(R.id.headerrightButton);
        headerTxtItemInCard = (TextView)listCartDialog.findViewById(R.id.headerText);
        expListClearLayout = (RelativeLayout) listCartDialog.findViewById(R.id.expListClearLayout);
        clearExpandableListView = (ExpandableListView)listCartDialog.findViewById(R.id.clearItemList);
        clearExpandList=getClearData();
        clearexpanbleListTitle = new ArrayList<String>(clearExpandList.keySet());
        clearExpandableListAdpater = new CustomExpandableListAdapter(mContext, clearexpanbleListTitle, clearExpandList,typeface);
        clearExpandableListView.setAdapter(clearExpandableListAdpater);
        headerTextClearOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isClearExpanded)
                    expandClearExpandableList();
                else
                    resetClearExpandableList();
            }
        });
        clearExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                final String selected = (String) clearExpandableListAdpater.getChild(
                        groupPosition, childPosition);
                if (CLEAR_ALL.equals(selected)) {
                    resetOrderList();
                    resetAll();
                }
                setButtonBgResetItemCancel();
                clearexpanbleListTitle.set(groupPosition,selected);
                clearExpandableListAdpater = new CustomExpandableListAdapter(mContext, clearexpanbleListTitle, clearExpandList,typeface);
                clearExpandableListView.setAdapter(clearExpandableListAdpater);
                expListClearLayout.setBackgroundColor(Color.TRANSPARENT);
                expListClearLayout.setLayoutParams(LayoutCollapse);
                listCartDialog.dismiss();
                return false;
            }
        });
        clearExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                setButtonBgReadyToItemSelected();
            }
        });
        clearExpandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                setButtonBgResetItemCancel();
            }
        });
        popupClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setButtonBgResetItemCancel();
                expListClearLayout.setBackgroundColor(Color.TRANSPARENT);
                expListClearLayout.setLayoutParams(LayoutCollapse);
                listCartDialog.dismiss();
            }
        });
        headerTxt=getHeaderText();
        btAdapViewOrderItem=getBtToViewOrderItem();
        headerTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOrderedItemInSquenceList();
            }
        });
        //this line is added to get the proper list over the view orderlist
        btAdapViewOrderItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewOrderedItemInSquenceList();
            }
        });
        expListClearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                resetClearExpandableList();
                setButtonBgResetItemCancel();
                expListClearLayout.setBackgroundColor(Color.TRANSPARENT);
                expListClearLayout.setLayoutParams(LayoutCollapse);
                return false;
            }
        });
        if (ZERO == position){
            stringBuilder=new StringBuilder();
            finalBuilder= new StringBuilder();
            quickSaleTransaction();
        }
        if (ONE == position) {
            search = (SearchView) layout.findViewById(R.id.search);
            int id = search.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
            TextView searchText = (TextView) search.findViewById(id);
            searchText.setTypeface(setTypeface);
            expandableListView = (ExpandableListView) layout.findViewById(R.id.lvExp);
            expListLayout = (RelativeLayout) layout.findViewById(R.id.expListLayout);
            search.setOnSearchClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    expandableListView.setVisibility(View.GONE);
                }
            });
            search.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    expandableListView.setVisibility(View.VISIBLE);
                    return false;
                }
            });
            expandableListDetail = getData();
            expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
            expandableListAdapter = new CustomExpandableListAdapter(mContext, expandableListTitle, expandableListDetail,typeface);
            expandableListView.setAdapter(expandableListAdapter);
            expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    final String selected = (String) expandableListAdapter.getChild(
                            groupPosition, childPosition);
                    if (ALL_ITEMS.equals(selected)) {
                        expandableListDetail = getData();
                        setRecyclerViewLibrary();
                    }else {
                        expandableListDetail = getDataAfter();
                        setRecyclerViewDiscount();
                    }
                    expandableListTitle.set(groupPosition,selected);
                    expandableListAdapter = new CustomExpandableListAdapter(mContext, expandableListTitle, expandableListDetail,typeface);
                    expandableListView.setAdapter(expandableListAdapter);
                    search.setVisibility(View.VISIBLE);
                    expandableListView.setPadding(0,0,100,0);
                    expListLayout.setBackgroundColor(Color.TRANSPARENT);
                    expListLayout.setLayoutParams(LayoutCollapse);
                    return false;
                }
            });
            expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    expandableListView.setPadding(0,0,0,0);
                    search.setVisibility(View.GONE);
                    expListLayout.setBackgroundColor(Color.parseColor(TRANSLUCENT));
                    expListLayout.setLayoutParams(LayoutExpand);
                }
            });
            expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
                @Override
                public void onGroupCollapse(int groupPosition) {
                    search.setVisibility(View.VISIBLE);
                    expandableListView.setPadding(0,0,100,0);
                    expListLayout.setBackgroundColor(Color.TRANSPARENT);
                    expListLayout.setLayoutParams(LayoutCollapse);
                }
            });
            expListLayout.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    resetExpandableList();
                    return false;
                }
            });

            recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerview);
            itemListAdapter = new ItemListAdapter(itemList,setTypeface, new ItemListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(Item item) {
                    //Toast.makeText(mContext,"item index "+item.getItemName(),Toast.LENGTH_SHORT).show();
                    sumOfLibrarySale(item.getItemName(),item.getPrice());
                }
            });
            itemListAdapter.setContext(mContext);
            mLayoutManager = new LinearLayoutManager(mContext);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(itemListAdapter);
            listener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    newText = newText.toLowerCase();
                    final List<Item> filteredList = new ArrayList<>();
                    for (int i=0;i<itemList.size();i++){
                    final String text = itemList.get(i).getItemName().toLowerCase();
                        if (text.contains(newText))
                            filteredList.add(itemList.get(i));
                    }
                    itemListAdapter = new ItemListAdapter(filteredList,setTypeface, new ItemListAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(Item item) {
                            //Toast.makeText(mContext,"item index "+item.getItemName(),Toast.LENGTH_SHORT).show();
                            sumOfLibrarySale(item.getItemName(),item.getPrice());
                        }
                    });
                    itemListAdapter.setContext(mContext);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(itemListAdapter);
                    itemListAdapter.notifyDataSetChanged();  // data set changed
                    return true;
                }
            };
            search.setOnQueryTextListener(listener); // call the QuerytextListner.
            dataAddedInList();
        }
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return ModelObject.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        ModelObject customPagerEnum = ModelObject.values()[position];

        return mContext.getString(customPagerEnum.getTitleResId());
    }
    private void setRecyclerViewLibrary(){
        itemListAdapter.swap(itemList);
        libraryAddedInList();
        itemListAdapter = new ItemListAdapter(itemList,setTypeface, new ItemListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Item item) {
                //Toast.makeText(mContext,"item index "+item.getItemName(),Toast.LENGTH_SHORT).show();
                sumOfLibrarySale(item.getItemName(),item.getPrice());
            }
        });
        itemListAdapter.setContext(mContext);
        mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(itemListAdapter);
    }
    private void setRecyclerViewDiscount(){
        itemListAdapter.swap(itemList);
        disCountList();
        itemListAdapter = new ItemListAdapter(itemList,setTypeface, new ItemListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Item item) {
                //Toast.makeText(mContext,"item index "+item.getItemName(),Toast.LENGTH_SHORT).show();
                subtractingDiscount(item.getItemName(),item.getPrice());
            }
        });
        itemListAdapter.setContext(mContext);
        mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(itemListAdapter);
    }
    private  void dataAddedInList(){
        for (int i=0;i<itemname.length;i++){
            Item itemData = new Item(imageName[i],imageIdexing[i],indexing[i],itemname[i], price[i], quatity[i]);
            itemList.add(itemData);
        }
    }
    private  void libraryAddedInList(){
        itemList =  new ArrayList<>();
        for (int i=0;i<itemname.length;i++){
            Item itemData = new Item(imageName[i],imageIdexing[i],indexing[i],itemname[i], price[i], quatity[i]);
            itemList.add(itemData);
        }
    }
    private void disCountList(){
        itemList =  new ArrayList<>();
        for (int i=0;i<disItemname.length;i++){
            Item itemData = new Item(disImageName[i],imageIdexing[i],disIndexing[i],disItemname[i], disPrice[i], disQuatity[i]);
            itemList.add(itemData);
        }
    }
    private void orderEditInCartSequeceMode(String itemName,String customNote,String price,String qty,String saleType,String isSaleTaxable,int position){
        //old code for edited list
        itemOrderList =  new ArrayList<>();
        int alteredItemCount=ZERO;
        double alterItemTotalAmt =ZERO;
        String alItemname=EMPTY,alItemCustomeNote=EMPTY,alItemPrice=EMPTY,alItemQuatity=EMPTY,alItemSaleType=EMPTY,alItemTaxable=EMPTY;
        if (!ST_ZERO.equals(qty)) {
            fItemName.set(position, itemName);
            fItemCustomNote.set(position,customNote);
            fItemPrice.set(position, Util.inrCurrencyFormat(String.valueOf(price)));
            fItemQty.set(position, qty);
            fSaleType.set(position, saleType);
            fSaleItemTaxable.add(position,isSaleTaxable);
        }else{
            fItemName.remove(position);
            fItemCustomNote.remove(position);
            fItemPrice.remove(position);
            fItemQty.remove(position);
            fSaleType.remove(position);
            fSaleItemTaxable.remove(position);
        }
        //new code realter the discount field
        int actualItemSize= fItemName.size();
        int dynamicItemSize=0;
        dynamicItemSize=actualItemSize;
        int disCount=0;
        String[] ItemDisName=null;
        String[] ItemDisAmount=null;
        //discount order list calculation
        String itemDiscount = Util.getData_String(ITEM_DISCOUNT,getmAppContext());
        String itemDiscountAmt = Util.getData_String(ITEM_DISCOUNT_AMT,getmAppContext());
        if (!EMPTY.equals(itemDiscount)) {
            ItemDisName = itemDiscount.split(TILDE);
            ItemDisAmount = itemDiscountAmt.split(TILDE);
            dynamicItemSize+=ItemDisName.length;
        }
        for (int i=0;i<dynamicItemSize;i++){
            if (i<actualItemSize) {
                String tempsingleItemPrice = fItemPrice.get(i).replace(COMMA, EMPTY).replace(DOLLAR, EMPTY);
                String tempSignleItemQty = fItemQty.get(i);
                Double singleItemPrice = Double.parseDouble(tempsingleItemPrice);
                int singleItemQty = Integer.parseInt(tempSignleItemQty);
                alteredItemCount += singleItemQty;
                alterItemTotalAmt += Double.parseDouble(String.valueOf(singleItemPrice * singleItemQty));
                alItemname += fItemName.get(i) + TILDA;
                alItemCustomeNote += fItemCustomNote.get(i) + TILDA;
                alItemPrice += DOLLAR + fItemPrice.get(i) + TILDA;
                alItemQuatity += tempSignleItemQty + TILDA;
                alItemSaleType += fSaleType.get(i) + TILDA;
                alItemTaxable += fSaleItemTaxable.get(i) + TILDA;
                itemOrderList.add(new ItemData(fItemName.get(i), fItemCustomNote.get(i), DOLLAR + Util.inrCurrencyFormat(fItemPrice.get(i).replace(COMMA, EMPTY)), tempSignleItemQty, fSaleType.get(i), fSaleItemTaxable.get(i)));
            }else{
                String discountedAmt =ItemDisAmount[disCount].toString();
                discountedAmt=discountedAmt.replace(COMMA,EMPTY).replace(DOLLAR,EMPTY);
                itemOrderList.add(new ItemData(ItemDisName[disCount].toString(), NO_DATA_AVAIL, DOLLAR + Util.inrCurrencyFormat(discountedAmt),ST_ONE, DISCOUNT_ON_SALE, NOT_TAXABLE));
                disCount++;
            }
        }
        Util.setData_String(ITEM_NAME,alItemname,getmAppContext());
        Util.setData_String(ITEM_CUSTOM_NOTE,alItemCustomeNote,getmAppContext());
        Util.setData_String(ITEM_AMOUNT,alItemPrice,getmAppContext());
        Util.setData_String(ITEM_QUATITY,alItemQuatity,getmAppContext());
        Util.setData_String(ITEM_SALE_TYPE,alItemSaleType,getmAppContext());
        Util.setData_String(ITEM_TAXABLE,alItemTaxable,getmAppContext());
        double disAmt = Double.parseDouble(Util.getData_String(TOTAL_DISCOUNT_ON_ITEM,getmAppContext()));
        alterItemTotalAmt=alterItemTotalAmt-disAmt;
        Util.setData_String(ALTER_ITEM_PRICE_SUM,String.valueOf(alterItemTotalAmt),mAppContext);
        Util.setData_String(ALTER_ITEM_QUATITY_SUM,String.valueOf(alteredItemCount),mAppContext);
        String updatedTotalAmount =Util.inrCurrencyFormat(String.valueOf(alterItemTotalAmt));
        if (itemOrderList.size()!=ZERO){
            headerQuatityDis.setText(String.valueOf(alteredItemCount));
            headerQuatityDis.setVisibility(View.VISIBLE);
            headerText.setText(CURRENT_SALE);
            quickSaleAmt.setText(CHARGE+ONE_LINE_SPACE+DOLLAR+updatedTotalAmount);
            headerTxtItemInCard.setText(ITEM_ORDER_TAG+DOLLAR+updatedTotalAmount);
            Util.setData_String(ITEM_TOTAL_AMT,updatedTotalAmount,getmAppContext());

        }else{
            Util.resetSharedPreference(mAppContext);
            headerQuatityDis.setText(ST_ZERO);
            headerQuatityDis.setVisibility(View.GONE);
            headerText.setText(NO_SALE);
            quickSaleAmt.setText(INITIAL);
            headerTxtItemInCard.setText(ITEM_ORDER_TAG+DOLLAR+updatedTotalAmount);
        }
    }
    private void orderInCartSequenceMode(){
        int actualItemSize= fItemName.size();
        int dynamicItemSize=0;
        dynamicItemSize=actualItemSize;
        int disCount=0;
        String[] ItemDisName=null;
        String[] ItemDisAmount=null;
        //discount order list calculation
        String itemDiscount = Util.getData_String(ITEM_DISCOUNT,getmAppContext());
        String itemDiscountAmt = Util.getData_String(ITEM_DISCOUNT_AMT,getmAppContext());
        if (!EMPTY.equals(itemDiscount)) {
            ItemDisName = itemDiscount.split(TILDE);
            ItemDisAmount = itemDiscountAmt.split(TILDE);
            dynamicItemSize+=ItemDisName.length;
        }
        //old code for generate the list of order item list
        itemOrderList =  new ArrayList<>();
        for (int i=0;i<dynamicItemSize;i++){
            if (i<actualItemSize)
                itemOrderList.add(new ItemData(fItemName.get(i),fItemCustomNote.get(i),DOLLAR+Util.inrCurrencyFormat(fItemPrice.get(i).replace(COMMA,EMPTY)), fItemQty.get(i),fSaleType.get(i),fSaleItemTaxable.get(i)));
            else {
                String discountedAmt =ItemDisAmount[disCount].toString();
                discountedAmt=discountedAmt.replace(COMMA,EMPTY).replace(DOLLAR,EMPTY);
                itemOrderList.add(new ItemData(ItemDisName[disCount].toString(), NO_DATA_AVAIL, DOLLAR + Util.inrCurrencyFormat(discountedAmt),ST_ONE, DISCOUNT_ON_SALE, NOT_TAXABLE));
                disCount++;
            }
        }
    }
    private  void quickSaleTransaction(){
        dialogItem = getDialog();
        clearDialog=getClearItemDialog();
        pCancelItem =(TextView) clearDialog.findViewById(R.id.declineButton);
        pClearItem =(TextView) clearDialog.findViewById(R.id.acceptButton);
        popupCloseButton = (TextView) dialogItem.findViewById(R.id.headerleftButton);
        popAddButton = (TextView)dialogItem.findViewById(R.id.headerrightButton);
        addNoteText = (EditText)dialogItem.findViewById(R.id.addnote);
        quickSaleAmount= (TextView)layout.findViewById(R.id.cal_text);
        quickSaleTag=(TextView)layout.findViewById(R.id.note_text);
        headerQuatity=getHeaderQuatityDis();
        quickSaleAmt=getQuickSaleAmtDisplay();
        one=(Button)layout.findViewById(R.id.one);
        two=(Button)layout.findViewById(R.id.two);
        three=(Button)layout.findViewById(R.id.three);
        four=(Button)layout.findViewById(R.id.four);
        five=(Button)layout.findViewById(R.id.five);
        six=(Button)layout.findViewById(R.id.six);
        seven=(Button)layout.findViewById(R.id.seven);
        eight=(Button)layout.findViewById(R.id.eight);
        nine=(Button)layout.findViewById(R.id.nine);
        zero=(Button)layout.findViewById(R.id.zero);
        cancel=(Button)layout.findViewById(R.id.cancel);
        plus=(Button)layout.findViewById(R.id.plus);

        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        seven.setOnClickListener(this);
        eight.setOnClickListener(this);
        nine.setOnClickListener(this);
        zero.setOnClickListener(this);
        cancel.setOnClickListener(this);
        cancel.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showDialog();
                return false;
            }
        });
        plus.setOnClickListener(this);
        quickSaleAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int len= quickSaleAmount.getText().length();
                String availQuat = Util.getData_String(ITEM_QUATITY,getmAppContext());
                if (!INITIAL.equals(quickSaleAmount.getText().toString()) && !ST_ZERO.equals(availQuat)){
                    headerText.setText(CURRENT_SALE);
                    String saleAmount = quickSaleAmount.getText().toString();
                    quickSaleAmt.setText(CHARGE+ONE_LINE_SPACE+saleAmount);
                }
                if (INITIAL.equals(quickSaleAmount.getText().toString())&& ST_ZERO.equals(availQuat)){
                        headerQuatityDis.setText(ST_ZERO);
                        headerQuatityDis.setVisibility(View.GONE);
                        headerText.setText(NO_SALE);
                        quickSaleAmt.setText(INITIAL);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        quickSaleTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tagValue = quickSaleTag.getText().toString();
                if (!ADD_NOTE.equals(tagValue))
                    addNoteText.setText(tagValue);
                dialogItem.show();
            }
        });
        popupCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNoteText.setText(EMPTY);
                dialogItem.dismiss();
            }
        });
        popAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addNoteText.getText().length()>ZERO) {
                    quickSaleTag.setText(addNoteText.getText());
                    addNoteText.setText(EMPTY);
                }else
                    quickSaleTag.setText(ADD_NOTE);
                dialogItem.dismiss();
            }
        });
        pCancelItem.setTypeface(setTypeface);
        pClearItem.setTypeface(setTypeface);
        popupCloseButton.setTypeface(setTypeface);
        popAddButton.setTypeface(setTypeface);
        addNoteText.setTypeface(setTypeface);
        quickSaleAmount.setTypeface(setTypeface);
        quickSaleTag.setTypeface(setTypeface);
        headerTxt.setTypeface(setTypeface);
        headerQuatity.setTypeface(setTypeface);
        quickSaleAmt.setTypeface(setTypeface);
        one.setTypeface(setTypeface);
        two.setTypeface(setTypeface);
        three.setTypeface(setTypeface);
        four.setTypeface(setTypeface);
        five.setTypeface(setTypeface);
        six.setTypeface(setTypeface);
        seven.setTypeface(setTypeface);
        eight.setTypeface(setTypeface);
        nine.setTypeface(setTypeface);
        zero.setTypeface(setTypeface);
        cancel.setTypeface(setTypeface);
        plus.setTypeface(setTypeface);
    }
    public View.OnClickListener quickSaleRateEdit = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.one:
                    quickSaleEditRate(ST_ONE);
                    break;
                case R.id.two:
                    quickSaleEditRate(ST_TWO);
                    break;
                case R.id.three:
                    quickSaleEditRate(ST_THREE);
                    break;
                case R.id.four:
                    quickSaleEditRate(ST_FOUR);
                    break;
                case R.id.five:
                    quickSaleEditRate(ST_FIVE);
                    break;
                case R.id.six:
                    quickSaleEditRate(ST_SIX);
                    break;
                case R.id.seven:
                    quickSaleEditRate(ST_SEVEN);
                    break;
                case R.id.eight:
                    quickSaleEditRate(ST_EIGHT);
                    break;
                case R.id.nine:
                    quickSaleEditRate(ST_NINE);
                    break;
                case R.id.zero:
                    quickSaleEditRate(ST_ZERO);
                    break;
                case R.id.cancel:
                    resetSingleQuickSaleRate();
                    break;
                default:
                    break;
            }
        }
    };

    public void onClick(View view){
        switch (view.getId()){
            case R.id.one:
                quickSaleSum(ST_ONE);
                break;
            case R.id.two:
                quickSaleSum(ST_TWO);
                break;
            case R.id.three:
                quickSaleSum(ST_THREE);
                break;
            case R.id.four:
                quickSaleSum(ST_FOUR);
                break;
            case R.id.five:
                quickSaleSum(ST_FIVE);
                break;
            case R.id.six:
                quickSaleSum(ST_SIX);
                break;
            case R.id.seven:
                quickSaleSum(ST_SEVEN);
                break;
            case R.id.eight:
                quickSaleSum(ST_EIGHT);
                break;
            case R.id.nine:
                quickSaleSum(ST_NINE);
                break;
            case R.id.zero:
                quickSaleSum(ST_ZERO);
                break;
            case R.id.cancel:
                resetQuickSale();
                break;
            case R.id.plus:
                quickSaleAdd();
                break;
            case R.id.note_text:
                break;
            default:
                break;
        }
    }
    public void resetExpandableList(){
        int count = expandableListAdapter.getGroupCount();
        for ( int i = 0; i < count; i++ )
            expandableListView.collapseGroup(i);
        expListLayout.setBackgroundColor(Color.TRANSPARENT);
        expListLayout.setLayoutParams(LayoutCollapse);
    }
    private void resetClearExpandableList(){
        int count = expandableListAdapter.getGroupCount();
        for ( int i = 0; i < count; i++ )
            clearExpandableListView.collapseGroup(i);
        expListClearLayout.setBackgroundColor(Color.TRANSPARENT);
        expListClearLayout.setLayoutParams(LayoutCollapse);
    }
    private void expandClearExpandableList(){
        int count = expandableListAdapter.getGroupCount();
        for ( int i = 0; i < count; i++ )
            clearExpandableListView.expandGroup(i);
        expListClearLayout.setBackgroundColor(Color.parseColor(TRANSLUCENT));
        expListClearLayout.setLayoutParams(LayoutExpand);
    }
    private void resetQuickSale(){
        stringBuilder=new StringBuilder();
        quickSaleAmount.setText(INITIAL);
        quickSaleTag.setText(ADD_NOTE);
        headerQuatityDis.setText(String.valueOf(Util.getData_Int(ITEM_COUNT,getmAppContext())));
        sumOfquickSale();
    }
    private void resetSingleQuickSaleRate(){
        qSaleSingleRateEdit=new StringBuilder();
        edqSaleRateCalText.setText(INITIAL);
    }
    private void showDialog(){
        clearDialog.show();
        pCancelItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearDialog.dismiss();
            }
        });
        pClearItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAll();
                if (itemOrderList.size()!=ZERO) {
                    itemAddToCartAdapter.swap(itemOrderList);
                    orderedRecyclerView.setAdapter(itemAddToCartAdapter);
                    itemAddToCartAdapter.notifyDataSetChanged();
                }
                clearDialog.dismiss();
            }
        });
    }
    private void resetAll(){
        stringBuilder=new StringBuilder();
        totalSumAmount=D_ZERO;
        quickSaleAmt.setText(INITIAL);
        quickSaleAmount.setText(INITIAL);
        quickSaleTag.setText(ADD_NOTE);
        headerQuatityDis.setText(ST_ZERO);
        headerQuatityDis.setVisibility(View.GONE);
        headerText.setText(NO_SALE);
        Util.setData_Int(ITEM_COUNT,ZERO,getmAppContext());
        Util.setData_String(ITEM_NAME, EMPTY, getmAppContext());
        Util.setData_String(ITEM_CUSTOM_NOTE,EMPTY, getmAppContext());
        Util.setData_String(ITEM_AMOUNT,EMPTY,getmAppContext());
        Util.setData_String(ITEM_QUATITY,ST_ZERO,getmAppContext());
        Util.setData_String(ITEM_DISCOUNT,EMPTY,getmAppContext());
        Util.setData_String(ITEM_DISCOUNT,EMPTY,getmAppContext());
        Util.setData_String(ITEM_DISCOUNT_AMT,EMPTY,getmAppContext());
        Util.setData_String(ITEM_SALE_TYPE,EMPTY,getmAppContext());
        Util.setData_String(TOTAL_DISCOUNT_ON_ITEM,ST_ZERO,getmAppContext());
    }
    private void quickSaleSum(String value){
        if (quickSaleAmount.getText() != null) {
            if (quickSaleAmount.getText().length() < ELEVEN) {
                quat = Util.getData_Int(ITEM_COUNT, getmAppContext());
                quat += ONE;
                headerQuatityDis.setVisibility(View.VISIBLE);
                headerQuatityDis.setText(String.valueOf(quat));
                headerText.setText(CURRENT_SALE);
                quickSaleAmount.setText(EMPTY);
                stringBuilder.append(value);
                quickSaleAmount.setText(DOLLAR + Util.indianCurrencyFormat(stringBuilder.toString()));
            } else {
                quickSaleAmount.setText(FINALAMT);
            }
            sumOfquickSale();
        }
    }
    private void quickSaleEditRate(String value){
        if (edqSaleRateCalText.getText()!=null){
            if (edqSaleRateCalText.getText().length() < ELEVEN) {
                qSaleSingleRateEdit.append(value);
                edqSaleRateCalText.setText(DOLLAR + Util.indianCurrencyFormat(qSaleSingleRateEdit.toString()));
            }else
                edqSaleRateCalText.setText(FINALAMT);
        }
    }
    private void sumOfquickSale(){
        Double totalSum = totalSumAmount + Double.parseDouble(quickSaleAmount.getText().toString().replace(COMMA, EMPTY).replace(DOLLAR,EMPTY).trim());
        quickSaleAmt.setText(CHARGE + ONE_LINE_SPACE + DOLLAR + Util.indianCurrencyFormat(String.valueOf(totalSum)));
        Util.setData_String(ITEM_TOTAL_AMT,quickSaleAmt.getText().toString(),getmAppContext());
    }
    private void quickSaleAdd(){
        String tagText = quickSaleTag.getText().toString();
        String itemAmt = quickSaleAmount.getText().toString();
        if (!INITIAL.equals(itemAmt)) {
            if (ADD_NOTE.equals(tagText))
                tagText = CUSTOM_AMOUNT;
            if (quickSaleAmt.getText()!=null && !INITIAL.equals(quickSaleAmt.getText().toString()))
                totalSumAmount=Double.parseDouble(quickSaleAmt.getText().toString().replace(DOLLAR,EMPTY).replace(COMMA,EMPTY).trim().replace(CHARGE,EMPTY).trim());
            String qItemName= Util.getData_String(ITEM_NAME,getmAppContext());
            String qItemCustomNote= Util.getData_String(ITEM_CUSTOM_NOTE,getmAppContext());
            String qItemAmount = Util.getData_String(ITEM_AMOUNT,getmAppContext());
            String qItemQuatity = Util.getData_String(ITEM_QUATITY,getmAppContext());
            if (ST_ZERO.equals(qItemQuatity))
                qItemQuatity="";
            String qSaleType = Util.getData_String(ITEM_SALE_TYPE,getmAppContext());
            String isTaxableItem =Util.getData_String(IS_ITEM_TAXABLE,getmAppContext());
            String qSaleTaxable=Util.getData_String(ITEM_TAXABLE,getmAppContext());
            Util.setData_Int(ITEM_COUNT,quat,getmAppContext());
            Util.setData_String(ITEM_NAME, qItemName+tagText + TILDA, getmAppContext());
            Util.setData_String(ITEM_CUSTOM_NOTE, qItemCustomNote+NO_DATA_AVAIL + TILDA, getmAppContext());
            Util.setData_String(ITEM_AMOUNT, qItemAmount+itemAmt + TILDA,getmAppContext());
            Util.setData_String(ITEM_QUATITY,qItemQuatity+ST_ONE+TILDA,getmAppContext());
            Util.setData_String(ITEM_SALE_TYPE,qSaleType+QUICK_SALE+TILDA,getmAppContext());
            Util.setData_String(ITEM_TAXABLE,qSaleTaxable+isTaxable(isTaxableItem)+TILDA,getmAppContext());
            Util.setData_String(ITEM_TOTAL_AMT,quickSaleAmt.getText().toString(),getmAppContext());
            resetQuickSale();
        }
    }
    private void sumOfLibrarySale(String orderedItem,String orderedPrice){
        totalSumAmount=Double.parseDouble(quickSaleAmt.getText().toString().replace(DOLLAR,EMPTY).replace(COMMA,EMPTY).trim().replace(CHARGE,EMPTY).trim());
        Double totalSum = totalSumAmount + Double.parseDouble(orderedPrice);
        quickSaleAmt.setText(CHARGE + ONE_LINE_SPACE + DOLLAR + Util.inrCurrencyFormat(String.valueOf(totalSum)));
        quat = Util.getData_Int(ITEM_COUNT, getmAppContext());
        quat += ONE;
        headerQuatityDis.setVisibility(View.VISIBLE);
        headerQuatityDis.setText(String.valueOf(quat));
        headerText.setText(CURRENT_SALE);
        String lItemName= Util.getData_String(ITEM_NAME,getmAppContext());
        String lItemCustomNote= Util.getData_String(ITEM_CUSTOM_NOTE,getmAppContext());
        String lItemAmount = Util.getData_String(ITEM_AMOUNT,getmAppContext());
        String lItemQuatity = Util.getData_String(ITEM_QUATITY,getmAppContext());
        if (ST_ZERO.equals(lItemQuatity))
            lItemQuatity="";
        String lSaleType = Util.getData_String(ITEM_SALE_TYPE,getmAppContext());
        String isTaxableItem =Util.getData_String(IS_ITEM_TAXABLE,getmAppContext());
        String lSaleTaxable=Util.getData_String(ITEM_TAXABLE,getmAppContext());
        Util.setData_Int(ITEM_COUNT,quat,getmAppContext());
        Util.setData_String(ITEM_NAME, lItemName+orderedItem + TILDA, getmAppContext());
        Util.setData_String(ITEM_CUSTOM_NOTE, lItemCustomNote+NO_DATA_AVAIL + TILDA, getmAppContext());
        Util.setData_String(ITEM_AMOUNT, lItemAmount+orderedPrice+ TILDA,getmAppContext());
        Util.setData_String(ITEM_QUATITY,lItemQuatity+ST_ONE+TILDA,getmAppContext());
        Util.setData_String(ITEM_SALE_TYPE,lSaleType+LIBRARY_SALE+TILDA,getmAppContext());
        Util.setData_String(ITEM_TAXABLE,lSaleTaxable+isTaxable(isTaxableItem)+TILDA,getmAppContext());
        Util.setData_String(ITEM_TOTAL_AMT,quickSaleAmt.getText().toString(),getmAppContext());

    }
    private void subtractingDiscount(String orderedItem,String orderedPrice){
        totalSumAmount=Double.parseDouble(quickSaleAmt.getText().toString().replace(DOLLAR,EMPTY).replace(COMMA,EMPTY).trim().replace(CHARGE,EMPTY).trim());
        String discountedItem = Util.getData_String(ITEM_DISCOUNT,getmAppContext());
        Double totalSum=0.0;
        Double totalDiscount= 0.0;
        Double tempDiscount=0.0;
        if (totalSumAmount>ZERO && !discountedItem.contains(orderedItem)) {
            tempDiscount = Double.parseDouble(orderedPrice.replace(MINUS, EMPTY));
            totalSum = totalSumAmount - tempDiscount;
            totalDiscount += tempDiscount;
            Util.setData_String(TOTAL_DISCOUNT_ON_ITEM, String.valueOf(totalDiscount), getmAppContext());
            if (totalSum>0)
                quickSaleAmt.setText(CHARGE + ONE_LINE_SPACE + DOLLAR + Util.inrCurrencyFormat(String.valueOf(totalSum)));
            String itemDiscount = Util.getData_String(ITEM_DISCOUNT,getmAppContext());
            String itemDiscountAmt = Util.getData_String(ITEM_DISCOUNT_AMT,getmAppContext());
            Util.setData_String(ITEM_DISCOUNT,itemDiscount+orderedItem+TILDA,getmAppContext());
            Util.setData_String(ITEM_DISCOUNT_AMT,itemDiscountAmt+orderedPrice+TILDA,getmAppContext());
        }
        headerQuatityDis.setVisibility(View.VISIBLE);
        headerText.setText(CURRENT_SALE);
    }
    private void editItemAddQuatity(){
        String currentItemName = Util.getData_String(SELECTED_ITEM_NAME,mAppContext);
        String qtyOfSingalItemSelected = edQuatity.getText().toString();
        if (EMPTY.equals(qtyOfSingalItemSelected))
            qtyOfSingalItemSelected=ST_ZERO;
        if(edQuatity.getText().length()<=SEVEN) {
            int quatity = Integer.parseInt(qtyOfSingalItemSelected);
            String amtOfSignalItemSelected = edOrderPrice.getText().toString();
            amtOfSignalItemSelected = amtOfSignalItemSelected.replace(COMMA, EMPTY).replace(DOLLAR, EMPTY);
            quatity += ONE;
            edQuatity.setText(String.valueOf(quatity));
            double amount = Double.parseDouble(amtOfSignalItemSelected);
            double totalAmtOfSingleItem = amount * quatity;
            pOrderListHeader.setText(currentItemName + ONE_LINE_SPACE + DOLLAR + Util.inrCurrencyFormat(String.valueOf(totalAmtOfSingleItem)));
        }
    }
    private void editItemRemoveQuatity(){
        String currentItemName = Util.getData_String(SELECTED_ITEM_NAME,mAppContext);
        String qtyOfSingalItemSelected = edQuatity.getText().toString();
        int quatity= Integer.parseInt(qtyOfSingalItemSelected);
        if (ZERO<quatity) {
            String amtOfSignalItemSelected = edOrderPrice.getText().toString();
            amtOfSignalItemSelected = amtOfSignalItemSelected.replace(COMMA, EMPTY).replace(DOLLAR, EMPTY);
            quatity-=ONE;
            edQuatity.setText(String.valueOf(quatity));
            double amount = Double.parseDouble(amtOfSignalItemSelected);
            double totalAmtOfSingleItem = amount * quatity;
            pOrderListHeader.setText(currentItemName + ONE_LINE_SPACE + DOLLAR + Util.inrCurrencyFormat(String.valueOf(totalAmtOfSingleItem)));
        }
    }
    private void editItemAddOrRemoveQuatityManually(){
        String currentItemName = Util.getData_String(SELECTED_ITEM_NAME,mAppContext);
        String qtyOfSingalItemSelected = edQuatity.getText().toString();
        if (EMPTY.equals(qtyOfSingalItemSelected))
            qtyOfSingalItemSelected=ST_ZERO;
        int quatity=0;
        if(edQuatity.getText().length()<=SEVEN) {
            quatity = Integer.parseInt(qtyOfSingalItemSelected);
            String amtOfSignalItemSelected = edOrderPrice.getText().toString();
            amtOfSignalItemSelected = amtOfSignalItemSelected.replace(COMMA, EMPTY).replace(DOLLAR, EMPTY);
            double amount = Double.parseDouble(amtOfSignalItemSelected);
            double totalAmtOfSingleItem = amount * quatity;
            pOrderListHeader.setText(currentItemName + ONE_LINE_SPACE + DOLLAR + Util.inrCurrencyFormat(String.valueOf(totalAmtOfSingleItem)));
        }
    }
    private void editItemQuickSaleRateSingleItem(){
        String currentItemName = Util.getData_String(SELECTED_ITEM_NAME,mAppContext);
        String priceOfSingalItemSelected = edOrderPrice.getText().toString();
        if (EMPTY.equals(priceOfSingalItemSelected) || INITIAL.equals(priceOfSingalItemSelected))
            priceOfSingalItemSelected=ST_ZERO;
            double price=0;
            price = Double.parseDouble(priceOfSingalItemSelected.replace(COMMA, EMPTY).replace(DOLLAR, EMPTY));
            String qtyOfSingalItemSelected = edQuatity.getText().toString();
            if (EMPTY.equals(qtyOfSingalItemSelected))
                qtyOfSingalItemSelected=ST_ZERO;
            else
                qtyOfSingalItemSelected = edQuatity.getText().toString();
            int qtyOfSignalItemSelected = Integer.parseInt(qtyOfSingalItemSelected);
            double totalAmtOfSingleItem = price * qtyOfSignalItemSelected;
            pOrderListHeader.setText(currentItemName + ONE_LINE_SPACE + DOLLAR + Util.inrCurrencyFormat(String.valueOf(totalAmtOfSingleItem)));
    }
    private void afterEditofItemCheckOnItemList(){
        String afterEditPrice=null;
        String afterEditTag=null;
        String afterEditQuatity=null;
        String isTaxable=null;
        boolean isTaxItem= Util.getData_Boolean(SINGLE_ITEM_TAX_CHK,mAppContext);
        String currentSaleType= Util.getData_String(CURRENT_ITEM_SALE_TYPE,mAppContext);
        if (edOrderPrice.getText().length()>ZERO)
            afterEditPrice = edOrderPrice.getText().toString().replace(COMMA,EMPTY).replace(DOLLAR,EMPTY);
        if (edOrderNoteAndQuality.getText().length()>ZERO) {
            afterEditTag = edOrderNoteAndQuality.getText().toString();
            if (EMPTY.equals(afterEditTag))
                afterEditTag=NO_DATA_AVAIL;
        }else
            afterEditTag=NO_DATA_AVAIL;
        if (edQuatity.getText().length()>ZERO)
            afterEditQuatity=edQuatity.getText().toString();
        if (isTaxItem)
            isTaxable=TAXABLE;
        else
            isTaxable=NOT_TAXABLE;
        int currentPosition= Util.getData_Int(CURRENT_ITEM_POSITION,mAppContext);
        String getCurrentName = Util.getData_String(SELECTED_ITEM_NAME,mAppContext);
        resetOrderList();
        orderEditInCartSequeceMode(getCurrentName,afterEditTag,afterEditPrice,afterEditQuatity,currentSaleType,isTaxable,currentPosition);
        alterOrderedListItem(itemOrderList);
        edOrderListWithTax.dismiss();
    }
    private void viewOrderedItemInSquenceList(){
        clearExpandList=getClearData();
        clearexpanbleListTitle = new ArrayList<String>(clearExpandList.keySet());
        clearExpandableListAdpater = new CustomExpandableListAdapter(mContext, clearexpanbleListTitle, clearExpandList,typeface);
        clearExpandableListView.setAdapter(clearExpandableListAdpater);
        String itemName= Util.getData_String(ITEM_NAME,getmAppContext());
        String itemAddNote= Util.getData_String(ITEM_CUSTOM_NOTE,getmAppContext());
        String itemAmount = Util.getData_String(ITEM_AMOUNT,getmAppContext());
        String itemQuatity = Util.getData_String(ITEM_QUATITY,getmAppContext());
        String itemSaleType = Util.getData_String(ITEM_SALE_TYPE,getmAppContext());
        String isitemTaxable =  Util.getData_String(ITEM_TAXABLE,getmAppContext());
        String itemDiscount = Util.getData_String(ITEM_DISCOUNT,getmAppContext());
        String itemDiscountAmt= Util.getData_String(ITEM_DISCOUNT_AMT,getmAppContext());
        String itemTotalAmt = Util.getData_String(ITEM_TOTAL_AMT,getmAppContext());
        double disAmt = Double.parseDouble(Util.getData_String(TOTAL_DISCOUNT_ON_ITEM,getmAppContext()));
        if (itemName!=null && !EMPTY.equals(itemName)&& itemAmount!=null && !EMPTY.equals(itemAmount) && itemQuatity!=null && !EMPTY.equals(itemQuatity)) {
            Double tempAmt = Double.parseDouble(itemTotalAmt.replace(CHARGE,EMPTY).replace(DOLLAR,EMPTY).replace(ONE_LINE_SPACE,EMPTY).replace(COMMA,EMPTY));
            tempAmt=tempAmt-disAmt;
            headerTxtItemInCard.setText(ITEM_ORDER_TAG+DOLLAR+Util.inrCurrencyFormat(String.valueOf(tempAmt)));
            String[] singleItem = itemName.split(TILDE);
            String[] singleItemÑote = itemAddNote.split(TILDE);
            String[] singleItemAmt = itemAmount.split(TILDE);
            String[] singleItemQty = itemQuatity.split(TILDE);
            String[] singleSaleType = itemSaleType.split(TILDE);
            String[] singleSaleItemTaxable = isitemTaxable.split(TILDE);
            itemOrderList = new ArrayList<>();
            for (int i = 0; i < singleItem.length; i++) {
                itemOrderList.add(new ItemData(singleItem[i],singleItemÑote[i], singleItemAmt[i], singleItemQty[i],singleSaleType[i],singleSaleItemTaxable[i]));
            }
            Collections.sort(itemOrderList, ItemData.ItemNameComparator);
            //Sorting exsisting item in card;
            fItemName = new ArrayList<String>();
            fItemCustomNote= new ArrayList<String>();
            fItemPrice = new ArrayList<String>();
            fItemQty = new ArrayList<String>();
            fSaleType =new ArrayList<String>();
            fSaleItemTaxable=new ArrayList<String>();
            sortingItemAddedInCart(itemOrderList);
            //adding the to recycle View ordered list
            orderedRecyclerView = (RecyclerView) listCartDialog.findViewById(R.id.summary_order_list);
            orderInCartSequenceMode();
            alterOrderedListItem(itemOrderList);
        }else
            headerTxtItemInCard.setText(ITEM_ORDER_TAG+INITIAL);
        listCartDialog.show();
        setButtonBgResetItemCancel();
    }
    private void alterOrderedListItem(List<ItemData> currentItemOrderList){
        itemAddToCartAdapter = new ItemAddToCartAdapter(currentItemOrderList, typeface, new ItemAddToCartAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ItemData itemData, int position) {
                //Toast.makeText(getmAppContext(), "item index " + itemData.getItemName()+" position : "+position, Toast.LENGTH_SHORT).show();
                if (!DISCOUNT_ON_SALE.equals(itemData.getSaleType())) {
                    edOrderListWithTax.show();
                    resetSingleItemDataDeleteButton();
                    Util.setData_Int(CURRENT_ITEM_POSITION, position, mAppContext);
                    String saleType = itemData.getSaleType();
                    Util.setData_String(CURRENT_ITEM_SALE_TYPE, saleType, mAppContext);
                    String isSaleTaxable = itemData.getIsTaxable();
                    if (LIBRARY_SALE.equals(saleType)) {
                        price_Layout.setVisibility(View.GONE);
                        price_Label_Layout.setVisibility(View.GONE);
                    } else {
                        price_Layout.setVisibility(View.VISIBLE);
                        price_Label_Layout.setVisibility(View.VISIBLE);
                    }
                    if (NOT_TAXABLE.equals(isSaleTaxable)) {
                        taxableLayoutOrderList.setVisibility(View.GONE);
                        label_Tax_Layout.setVisibility(View.GONE);
                    } else {
                        taxableLayoutOrderList.setVisibility(View.VISIBLE);
                        label_Tax_Layout.setVisibility(View.VISIBLE);
                    }
                    String tempPriceSingleItem = itemData.getPrice();
                    String tempQuatSignleItem = itemData.getQuatity();
                    tempPriceSingleItem = tempPriceSingleItem.replace(DOLLAR, EMPTY).replace(COMMA, EMPTY);
                    Double pOfSingalItem = Double.parseDouble(tempPriceSingleItem);
                    Double qOfSingalItem = Double.parseDouble(tempQuatSignleItem);
                    Double totalOfSingalItem = pOfSingalItem * qOfSingalItem;
                    String currentSelName = itemData.getItemName();
                    Util.setData_String(SELECTED_ITEM_NAME, currentSelName, mAppContext);
                    String singleItemPrice = Util.inrCurrencyFormat(String.valueOf(totalOfSingalItem));
                    pOrderListHeader.setText(currentSelName + ONE_LINE_SPACE + DOLLAR + singleItemPrice);
                    String tempChkCusNote = itemData.getCustomNode();
                    if (!NO_DATA_AVAIL.equals(tempChkCusNote))
                        edOrderNoteAndQuality.setText(tempChkCusNote);
                    else
                        edOrderNoteAndQuality.setText(EMPTY);
                    edOrderPrice.setText(itemData.getPrice());
                    edQuatity.setText(itemData.getQuatity());
                }
            }
        });
        itemAddToCartAdapter.setContext(getmAppContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getmAppContext());
        orderedRecyclerView.setLayoutManager(mLayoutManager);
        orderedRecyclerView.setItemAnimator(new DefaultItemAnimator());
        orderedRecyclerView.setAdapter(itemAddToCartAdapter);
        itemAddToCartAdapter.notifyDataSetChanged();
    }
    private String isTaxable(String isTaxable){
        if (EMPTY.equals(isTaxable))
            isTaxable=NOT_TAXABLE;
        else
            isTaxable=TAXABLE;
        return isTaxable;
    }
    private void resetOrderList(){
        if (itemOrderList.size()!=ZERO) {
            itemAddToCartAdapter.swap(itemOrderList);
            orderedRecyclerView.setAdapter(itemAddToCartAdapter);
            itemAddToCartAdapter.notifyDataSetChanged();
        }
    }
    private void resetSingleItemDataDeleteButton(){
        confrimAndRemoveItem.setVisibility(View.GONE);
        removeSeletedItem.setVisibility(View.VISIBLE);
    }
    public TextView getHeaderText() {
        return headerText;
    }

    public void setHeaderText(TextView headerText) {
        this.headerText = headerText;
    }

    public Button getBtToViewOrderItem() {
        return btToViewOrderItem;
    }

    public void setBtToViewOrderItem(Button btToViewOrderItem) {
        this.btToViewOrderItem = btToViewOrderItem;
    }

    public TextView getHeaderQuatityDis() {
        return headerQuatityDis;
    }

    public void setHeaderQuatityDis(TextView headerQuatityDis) {
        this.headerQuatityDis = headerQuatityDis;
    }

    public TextView getQuickSaleAmtDisplay() {
        return quickSaleAmtDisplay;
    }

    public void setQuickSaleAmtDisplay(TextView quickSaleAmtDisplay) {
        this.quickSaleAmtDisplay = quickSaleAmtDisplay;
    }

    public Context getmAppContext() {
        return mAppContext;
    }

    public void setmAppContext(Context mAppContext) {
        this.mAppContext = mAppContext;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public Dialog getClearItemDialog() {
        return clearItemDialog;
    }

    public void setClearItemDialog(Dialog clearItemDialog) {
        this.clearItemDialog = clearItemDialog;
    }

    public Dialog getListItemCartDialog() {
        return listItemCartDialog;
    }

    public void setListItemCartDialog(Dialog listItemCartDialog) {
        this.listItemCartDialog = listItemCartDialog;
    }

    public Dialog getEditOrderListWithTax() {
        return editOrderListWithTax;
    }

    public void setEditOrderListWithTax(Dialog editOrderListWithTax) {
        this.editOrderListWithTax = editOrderListWithTax;
    }

    public Dialog getEditQuickSaleRate() {
        return editQuickSaleRate;
    }

    public void setEditQuickSaleRate(Dialog editQuickSaleRate) {
        this.editQuickSaleRate = editQuickSaleRate;
    }

    public Typeface getTypeface() {
        return typeface;
    }

    public void setTypeface(Typeface typeface) {
        this.typeface = typeface;
    }
    public static HashMap<String, List<String>> getClearData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();
        List<String> chooseItem = new ArrayList<String>();
        chooseItem.add(CLEAR_ALL);
        expandableListDetail.put(EMPTY, chooseItem);
        return expandableListDetail;
    }
    protected void setButtonBgReadyToItemSelected(){
        isClearExpanded=true;
        headerTextClearOption.setBackgroundResource(R.drawable.img_uparrow);
        expListClearLayout.setBackgroundColor(Color.parseColor(TRANSLUCENT));
        expListClearLayout.setLayoutParams(LayoutExpand);
    }
    protected void setButtonBgResetItemCancel(){
        isClearExpanded=false;
        headerTextClearOption.setBackgroundResource(R.drawable.img_downarrow);
        expListClearLayout.setBackgroundColor(Color.TRANSPARENT);
        expListClearLayout.setLayoutParams(LayoutCollapse);
    }

    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();
        List<String> chooseItem = new ArrayList<String>();
        chooseItem.add(ALL_ITEMS);
        chooseItem.add(DISCOUNT);
        expandableListDetail.put(ALL_ITEMS, chooseItem);
        return expandableListDetail;
    }
    public static HashMap<String, List<String>> getDataAfter() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();
        List<String> chooseItem = new ArrayList<String>();
        chooseItem.add(ALL_ITEMS);
        chooseItem.add(DISCOUNT);
        expandableListDetail.put(DISCOUNT, chooseItem);
        return expandableListDetail;
    }
    public void resetSearchView(){
        search.setIconified(true);
    }
    protected void sortingItemAddedInCart(List<ItemData> itemOrderList){
        int count=0;
        finalItemName=null;
        itSaleNote=EMPTY;
        itPrice=0.0;
        itQuatity=0;
        itSaleType=null;
        itSaleItemTaxable=null;
        String temp=null;
        int listSize= itemOrderList.size();
        for (int i = 0; i < listSize; i++) {
            if(i<listSize-1)
                temp=itemOrderList.get(i+1).getItemName();
            else if(i<listSize)
                temp=itemOrderList.get(i).getItemName();
            if (temp.equals(itemOrderList.get(i).getItemName())) {
                finalItemName=temp;
                String tempPrice=itemOrderList.get(i).getPrice();
                tempPrice=tempPrice.replace(DOLLAR,EMPTY).replace(COMMA,EMPTY);
                itSaleNote=itemOrderList.get(i).getCustomNode();
                itPrice+=Double.parseDouble(tempPrice);
                itQuatity+=Double.parseDouble(itemOrderList.get(i).getQuatity());
                itSaleType=itemOrderList.get(i).getSaleType();
                itSaleItemTaxable=itemOrderList.get(i).getIsTaxable();
                if (i==listSize-1) {
                    fItemName.add(finalItemName);
                    fItemCustomNote.add(itSaleNote);
                    fItemPrice.add(Util.inrCurrencyFormat(String.valueOf(itPrice)));
                    fItemQty.add(String.valueOf(itQuatity));
                    fSaleType.add(itSaleType);
                    fSaleItemTaxable.add(itSaleItemTaxable);
                }
            }else{
                finalItemName=itemOrderList.get(i).getItemName();
                String tempPriceOne=itemOrderList.get(i).getPrice();
                tempPriceOne=tempPriceOne.replace(DOLLAR,EMPTY).replace(COMMA,EMPTY);;
                itSaleNote=itemOrderList.get(i).getCustomNode();;
                itPrice+=Double.parseDouble(tempPriceOne);
                itQuatity+=Double.parseDouble(itemOrderList.get(i).getQuatity());
                itSaleType=itemOrderList.get(i).getSaleType();
                itSaleItemTaxable=itemOrderList.get(i).getIsTaxable();
                fItemName.add(finalItemName);
                fItemCustomNote.add(itSaleNote);
                fItemPrice.add(String.valueOf(Util.inrCurrencyFormat(String.valueOf(itPrice))));
                fItemQty.add(String.valueOf(itQuatity));
                fSaleType.add(itSaleType);
                fSaleItemTaxable.add(itSaleItemTaxable);
                finalItemName=null;
                itSaleNote=EMPTY;
                itPrice=0.0;
                itQuatity=0;
                itSaleType=null;
                itSaleItemTaxable=null;
                if (i==listSize-1) {
                    finalItemName=temp;
                    String tempSaleType=itemOrderList.get(i+1).getSaleType();
                    String tempSaleTaxable =itemOrderList.get(i+1).getIsTaxable();
                    String tempSaleNoteTwo=itemOrderList.get(i+1).getCustomNode();
                    String tempPriceTwo=itemOrderList.get(i+1).getPrice();
                    tempPriceTwo=tempPriceTwo.replace(DOLLAR,EMPTY).replace(COMMA,EMPTY);;
                    itSaleNote=tempSaleNoteTwo;
                    itPrice+=Double.parseDouble(tempPriceTwo);
                    itQuatity+=Double.parseDouble(itemOrderList.get(i+1).getQuatity());
                    itSaleType=tempSaleType;
                    itSaleItemTaxable=tempSaleTaxable;
                }
                count++;
            }
        }
    }
}

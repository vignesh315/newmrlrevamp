package com.mrlpay.common;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.mrlpay.MainActivity;

import java.text.DecimalFormat;
import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;
import static com.mrlpay.constants.Message.ALTER_ITEM_PRICE_SUM;
import static com.mrlpay.constants.Message.ALTER_ITEM_QUATITY_SUM;
import static com.mrlpay.constants.Message.CURRENT_ITEM_POSITION;
import static com.mrlpay.constants.Message.CURRENT_ITEM_SALE_TYPE;
import static com.mrlpay.constants.Message.DEFAULTCURRENCYFORMAT;
import static com.mrlpay.constants.Message.EMPTY;
import static com.mrlpay.constants.Message.HUNDRED;
import static com.mrlpay.constants.Message.INDIANCURREFORMATONE;
import static com.mrlpay.constants.Message.INDIANCURRFORMAT;
import static com.mrlpay.constants.Message.IS_ITEM_TAXABLE;
import static com.mrlpay.constants.Message.ITEM_AMOUNT;
import static com.mrlpay.constants.Message.ITEM_COUNT;
import static com.mrlpay.constants.Message.ITEM_CUSTOM_NOTE;
import static com.mrlpay.constants.Message.ITEM_DISCOUNT;
import static com.mrlpay.constants.Message.ITEM_DISCOUNT_AMT;
import static com.mrlpay.constants.Message.ITEM_NAME;
import static com.mrlpay.constants.Message.ITEM_QUATITY;
import static com.mrlpay.constants.Message.ITEM_SALE_TYPE;
import static com.mrlpay.constants.Message.ITEM_TAXABLE;
import static com.mrlpay.constants.Message.ONE;
import static com.mrlpay.constants.Message.SELECTED_ITEM_NAME;
import static com.mrlpay.constants.Message.SINGLE_ITEM_TAX_CHK;
import static com.mrlpay.constants.Message.ST_ZERO;
import static com.mrlpay.constants.Message.TOTAL_DISCOUNT_ON_ITEM;
import static com.mrlpay.constants.Message.TWO;
import static com.mrlpay.constants.Message.ZERO;

/**
 * Created by Vignesh on 06-10-2016.
 */

public class Util {
    private PendingIntent pendingIntent;
    AlarmManager alarmManager;
    public static void setData_String(String stKey, String stValue, Context con) {
        SharedPreferences settings = null;
        SharedPreferences.Editor editor = null;
        settings = PreferenceManager.getDefaultSharedPreferences(con);
        editor = settings.edit();
        editor.putString(stKey, stValue).commit();

    }

    public static void setData_Boolean(String stKey, boolean bValue, Context con) {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(con);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(stKey, bValue).commit();
    }

    public static void setData_Int(String stKey, int iValue, Context con) {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(con);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(stKey, iValue).commit();
    }

    public static String getData_String(String stKey, Context con) {
        SharedPreferences settings1 = PreferenceManager
                .getDefaultSharedPreferences(con);
        return settings1.getString(stKey, "");
    }

    public static boolean getData_Boolean(String stKey, Context con) {
        SharedPreferences settings1 = PreferenceManager
                .getDefaultSharedPreferences(con);
        return settings1.getBoolean(stKey, false);
    }

    public static int getData_Int(String stKey, Context con) {
        SharedPreferences settings1 = PreferenceManager
                .getDefaultSharedPreferences(con);
        return settings1.getInt(stKey, 0);
    }
    public static void resetSharedPreference(Context con){
        Util.setData_Int(ITEM_COUNT,ZERO,con);
        Util.setData_String(ITEM_NAME, EMPTY, con);
        Util.setData_String(ITEM_CUSTOM_NOTE,EMPTY,con);
        Util.setData_String(ITEM_AMOUNT,EMPTY,con);
        Util.setData_String(ITEM_QUATITY,ST_ZERO,con);
        Util.setData_String(ITEM_SALE_TYPE,EMPTY,con);
        Util.setData_String(ITEM_TAXABLE,EMPTY,con);
        Util.setData_String(IS_ITEM_TAXABLE,EMPTY,con);
        Util.setData_Boolean(SINGLE_ITEM_TAX_CHK,false,con);
        Util.setData_Int(CURRENT_ITEM_POSITION,ZERO,con);
        Util.setData_String(SELECTED_ITEM_NAME,EMPTY,con);
        Util.setData_String(CURRENT_ITEM_SALE_TYPE,EMPTY,con);
        Util.setData_String(ALTER_ITEM_PRICE_SUM,EMPTY,con);
        Util.setData_String(ALTER_ITEM_QUATITY_SUM,EMPTY,con);
        Util.setData_String(ITEM_DISCOUNT,EMPTY,con);
        Util.setData_String(ITEM_DISCOUNT_AMT,EMPTY,con);
        Util.setData_String(TOTAL_DISCOUNT_ON_ITEM,ST_ZERO,con);
    }
    public static String inrCurrencyFormat(String data){
        DecimalFormat format = null;
        String resultData;
        format = new DecimalFormat(INDIANCURREFORMATONE);
        resultData = format.format(Double.parseDouble(data));
        return resultData;
    }
    public static String indianCurrencyFormat(String data) {
        DecimalFormat format = null;
        String finalResult;
        double formatToDouble = Double.parseDouble(data);
        int result = Double.compare(formatToDouble,ZERO);
        if (formatToDouble>ZERO &&  formatToDouble%ONE==ZERO && formatToDouble%TWO!=ZERO) {
            format = new DecimalFormat(INDIANCURRFORMAT);
            formatToDouble = formatToDouble / HUNDRED;
        }else{
            if (result!=ZERO) {
                if (formatToDouble % TWO == ZERO){
                    format = new DecimalFormat(INDIANCURREFORMATONE);
                    formatToDouble = formatToDouble / HUNDRED;
                }else
                    format = new DecimalFormat(INDIANCURREFORMATONE);
            }else
                format = new DecimalFormat(DEFAULTCURRENCYFORMAT);
        }
        finalResult = format.format(formatToDouble);
        return finalResult;
    }
}

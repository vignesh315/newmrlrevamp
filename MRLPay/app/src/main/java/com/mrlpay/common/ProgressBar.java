package com.mrlpay.common;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.jpardogo.android.googleprogressbar.library.ChromeFloatingCirclesDrawable;
import com.mrlpay.MainActivity;
import com.mrlpay.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Vignesh on 29-12-2016.
 */

public class ProgressBar extends Activity {
    @InjectView(R.id.google_progress)
    android.widget.ProgressBar mProgressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_progress);
        ButterKnife.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Rect bounds = mProgressBar.getIndeterminateDrawable().getBounds();
        mProgressBar.setIndeterminateDrawable(getProgressDrawable());
        mProgressBar.getIndeterminateDrawable().setBounds(bounds);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    private Drawable getProgressDrawable() {
        Drawable progressDrawable =  new ChromeFloatingCirclesDrawable.Builder(this)
                        .colors(getProgressDrawableColors())
                        .build();
        return progressDrawable;
    }

    private int[] getProgressDrawableColors() {
        int[] colors = new int[4];
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        colors[0] = prefs.getInt(getString(R.string.firstcolor_pref_key),getResources().getColor(R.color.red));
        colors[1] = prefs.getInt(getString(R.string.secondcolor_pref_key),getResources().getColor(R.color.blue));
        colors[2] = prefs.getInt(getString(R.string.thirdcolor_pref_key),getResources().getColor(R.color.yellow));
        colors[3] = prefs.getInt(getString(R.string.fourthcolor_pref_key), getResources().getColor(R.color.green));
        return colors;
    }
}

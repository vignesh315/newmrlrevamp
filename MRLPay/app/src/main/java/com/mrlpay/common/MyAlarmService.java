package com.mrlpay.common;

import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.mrlpay.R;
import com.mrlpay.activity.prelogin.LoginActivity;


public class MyAlarmService extends Service {
    @Override
    public void onCreate() {
        // TODO Auto-generated method stub

    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }



    @Override

    public void onDestroy() {

// TODO Auto-generated method stub

        super.onDestroy();

     //   Toast.makeText(this, "MyAlarmService.onDestroy()", Toast.LENGTH_LONG).show();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Context context=this;
        System.out.println("LoginActivity.session "+LoginActivity.session);
        if(LoginActivity.session)
            showDialogue();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // TODO Auto-generated method stub
        return super.onUnbind(intent);

    }
    public void showDialogue(){

        final Dialog dialog = new Dialog(MyAlarmService.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);

        // Set dialog title
        // dialog.setTitle("Custom Dialog");

        // set values for custom dialog components - text, image and button
        Typeface face= Typeface.createFromAsset(this.getAssets(), "fonts/calibri.otf");
        TextView text = (TextView) dialog.findViewById(R.id.textDialog);
        text.setTypeface(face);
        //text.setText("Custom dialog Android example.");
        TextView image = (TextView) dialog.findViewById(R.id.imageDialog);
        image.setTypeface(face);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView declineButton = (TextView) dialog.findViewById(R.id.declineButton);
        declineButton.setTypeface(face);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                Intent mainIntent = new Intent(MyAlarmService.this,
                        LoginActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(mainIntent);
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
    }
}